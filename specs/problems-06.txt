1. You are a developer for a university. Your current project is to develop a system for students to find courses they share with friends. The university has a system for querying courses students are enrolled in, returned as a list of (ID, course) pairs.

Write a function that takes in a list of (student ID number, course name) pairs and returns, for every pair of students, a list of all courses they share.

Sample Input:

student_course_pairs_1 = [
["58", "Linear Algebra"],
["94", "Art History"],
["94", "Operating Systems"],
["17", "Software Design"],
["58", "Mechanics"],
["58", "Economics"],
["17", "Linear Algebra"],
["17", "Political Science"],
["94", "Economics"],
["25", "Economics"],
["58", "Software Design"],
]

Sample Output (pseudocode, in any order):

find_pairs(student_course_pairs_1) =>
{
[58, 17]: ["Software Design", "Linear Algebra"]
[58, 94]: ["Economics"]
[58, 25]: ["Economics"]
[94, 25]: ["Economics"]
[17, 94]: []
[17, 25]: []
}

Additional test cases:

Sample Input:

student_course_pairs_2 = [
["42", "Software Design"],
["0", "Advanced Mechanics"],
["9", "Art History"],
]

Sample output:

find_pairs(student_course_pairs_2) =>
{
[0, 42]: []
[0, 9]: []
[9, 42]: []
}

n: number of pairs in the input
s: number of students
c: number of courses being offered
--------------------------------------------------------------------------------------------------
2. The image you get is known to have potentially many distinct rectangles of 0s on a background of 1's. Write a function that takes in the image and returns the coordinates of all the 0 rectangles -- top-left and bottom-right; or top-left, width and height.

image1 = [
[0, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 1, 1],
[0, 1, 1, 0, 0, 0, 1],
[1, 0, 1, 0, 0, 0, 1],
[1, 0, 1, 1, 1, 1, 1],
[1, 0, 1, 0, 0, 1, 1],
[1, 1, 1, 0, 0, 1, 1],
[1, 1, 1, 1, 1, 1, 0],
]

Sample output variations (only one is necessary):

findRectangles(image1) =>
// (using top-left-row-column and bottom-right):
[
[[0,0],[0,0]],
[[2,0],[2,0]],
[[2,3],[3,5]],
[[3,1],[5,1]],
[[5,3],[6,4]],
[[7,6],[7,6]],
]
// (using top-left-x-y and width/height):
[
[[0,0],[1,1]],
[[0,2],[1,1]],
[[3,2],[3,2]],
[[1,3],[1,3]],
[[3,5],[2,2]],
[[6,7],[1,1]],
]

Other test cases:

image2 = [
[0],
]

findRectangles(image2) =>
// (using top-left-row-column and bottom-right):
[
[[0,0],[0,0]],
]

// (using top-left-x-y and width/height):
[
[[0,0],[1,1]],
]

image3 = [
[1],
]

findRectangles(image3) => []

image4 = [
[1, 1, 1, 1, 1],
[1, 0, 0, 0, 1],
[1, 0, 0, 0, 1],
[1, 0, 0, 0, 1],
[1, 1, 1, 1, 1],
]

findRectangles(image4) =>
// (using top-left-row-column and bottom-right or top-left-x-y and width/height):
[
[[1,1],[3,3]],
]

n: number of rows in the input image
m: number of columns in the input image

--------------------------------------------------------------------------------------------------


3. All nodes have 0, 1, or multiple parent given the realitionship in pairs ((0,1),(1,2),(1,4)).
For example, (0,1) -> 0 is a parent of 1.

Find the node has no parent or exactly 1 parent.
Given two nodes decide do they have a common ancestor ?

--------------------------------------------------------------------------------------------------

4. How to segment a matrix by neighbouring values?

Input m = [0, 1, 1, 0,
          1, 1, 0, 0,
          0, 0, 0, 1]
          
Output           r = [[[0,0]],   
     [[0,1], [0,2], [1,0], [1,1]],   
     [[0,3], [1,2], [1,3], [2,0], [2,1], [2,2]]   
     [[2,3]]]  