"""
3. All nodes have 0, 1, or multiple parent given the realitionship in pairs ((0,1),(1,2),(1,4)).
For example, (0,1) -> 0 is a parent of 1.

Find the node has no parent or exactly 1 parent.
Given two nodes decide do they have a common ancestor ?
"""

######################################################################################################
"""
The following code has two parts - 
I. Find node that has no parent or exactly 1 parent - 
    1. Create a dictionary to store list of parents of each node.
    2. ITerate over the input adding each nodes parent to the dictionary.
    3. From the dictionary, filter nodes where length of parent is less than 2, and return
    
II. Given two nodes decide do they have a common ancestor
    1. Create a dictionary of nodes and their parents similar to I.
    2. For the two input nodes provided, recursively get a set of all their ancestors.
    3. Find the intersection of ancestors of the two nodes and return as output. 
"""
######################################################################################################


def find_single_parent_children(tree):
    """
    I. Method to find nodes with 0 or 1 parent
    :param tree: tuple of tuples containing the tree structure
    :return: list of nodes that have 0 or 1 parent
    """
    node_parents_map = dict()  # Initialize a dictionary to store the parents of each node in a list

    # Iterate over each relationship in the tree and add the parent to that node's parent's list
    for parent, node in tree:
        node_parents_map.setdefault(node, []).append(parent)

    single_or_no_parent_nodes = []  # Initialize list to store nodes with 0 or 1 parent

    # Iterate over all nodes in the dictionary and filter those with less than 2 parents, and add to the output list
    for node, parents in node_parents_map.items():
        if len(parents) < 2:
            single_or_no_parent_nodes.append(node)

    return single_or_no_parent_nodes


def extract_node_ancestors(node_parents_map, node):
    """
    II. Helper method to recursively get a list of all ancestors of a node from a map containing the node-parent
    relationship
    :param node_parents_map: Map containing each node and its parents as a list
    :param node: node whose ancestors are to be found
    :return: set of unique ancestors of the node
    """
    all_ancestors = []  # Initialize a list to store all ancestors of a node
    parents = node_parents_map.get(node, [])  # Get parents of the node if they exist in the map, else get an empty list

    for p in parents:
        all_ancestors.append(p)  # For each parent, add it to the ancestors
        parents_ancestors = extract_node_ancestors(node_parents_map, p)  # recursive call to get the parent's parents
        all_ancestors += parents_ancestors  # Add parent's ancestors to the node's ancestors
    return set(all_ancestors)  # Return a set of all ancestors


def find_common_ancestors(tree, nodes):
    """
    II. Method to find commons ancestors for a list of two nodes
    :param tree: tuple of tuples containing the tree structure
    :param nodes: list of two nodes whose common ancestors are to be extracted
    :return: list of common ancestors of the two input nodes
    """
    node_parents_map = dict()  # Initialize a dictionary to store the parents of each node in a list

    # Iterate over each relationship in the tree and add the parent to that node's parent's list
    for parent, node in tree:
        node_parents_map.setdefault(node, []).append(parent)

    node1, node2 = nodes  # Extract the two nodes

    # Get ancestors for both nodes in two sets
    node1_ancestors = extract_node_ancestors(node_parents_map, node1)
    node2_ancestors = extract_node_ancestors(node_parents_map, node2)

    # The intersection of the ancestors of the two nodes give the common ancestors.
    common_ancestors = node1_ancestors.intersection(node2_ancestors)

    return list(common_ancestors)


# Tests
print(find_single_parent_children(((0, 1), (1, 2), (1, 4))))  # [1, 2, 4]

print(find_common_ancestors(((0, 1), (1, 2), (1, 4)), [2, 4]))  # 0, 1
