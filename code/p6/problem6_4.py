"""
4. How to segment a matrix by neighbouring values?

Input m = [0, 1, 1, 0,
          1, 1, 0, 0,
          0, 0, 0, 1]

Output           r = [[[0,0]],
     [[0,1], [0,2], [1,0], [1,1]],
     [[0,3], [1,2], [1,3], [2,0], [2,1], [2,2]]
     [[2,3]]]
"""

######################################################################################################
"""
The following code segments a matrix in the following steps - 
1. Extract all 0 index and 1 index in two lists.
2. For every index in a list of indices, 
    a. Remove the index from list
    b. Check if any of the following elements(local neighbours) exists in the set of indices - 
        x+1, y -> Right element
        x-1, y -> Left element
        x, y-1 -> Top element
        x-1, y -> Bottom element
    c. If a neighbour exists, remove it from the list, and repeat step 2
3. Repeat step 2 for both lists from step 1, to get all segments with 0, and all segments with 1
"""
######################################################################################################


def segment_by_neighbours(m):
    """
    Method to segment a matrix
    :param m: matrix containing 0s and 1s
    :return: list of all segments in the matrix
    """

    # Find width and height of matrix to be used for iterating
    height = len(m)
    width = len(m[0])

    # Initialize empty lists to store indices of 0s and 1s in the matrix
    zero_indices = []
    one_indices = []

    # Iterate over all elements in the list and add it's index to either zero_indices or one_indices depending on
    #  it's value
    for i in range(height):
        for j in range(width):
            if m[i][j] == 0:
                zero_indices.append([i, j])
            else:
                one_indices.append([i, j])

    # Extract segments from both lists, join together, and return
    segments = get_segments(zero_indices) + get_segments(one_indices)
    return segments


def get_segments(indices):
    """
    Helper method to get segments, given a list of indices
    :param indices: list of indices
    :return: list containing segments formed by chaining the indices
    """
    segments = []  # Initialize empty list to hold segments

    # While there are elements in the indices list, extract it's neighbours and append to the output list
    while len(indices) > 0:
        curr_ind = indices[0]
        curr_seg = [curr_ind] + extract_neighbours(indices, curr_ind)
        segments.append(curr_seg)
    return segments


def extract_neighbours(indices, n):
    """
    Helper method to extract the list of valid neighbours given a list of all possible indices
    :param indices: list of possible indices for neighbour candidates
    :param n: current element for whom neighbours are to be extracted
    :return: list containing chain of neighbours of index n
    """
    indices.remove(n)  # Remove n from indices to remove backward propagation of neighbours
    x, y = n  # Extract x and y co-ordinates of the current index

    local_neighbours = []  # Initialize list to store immediate local neighbours

    # Check if immediate local neighbours(Right, Left, Bottom, Top) are present in list of indices and append to list
    # of local neighbours
    for poss_neighbour in [[x + 1, y], [x - 1, y], [x, y + 1], [x, y - 1]]:
        if poss_neighbour in indices:
            local_neighbours.append(poss_neighbour)
            indices.remove(poss_neighbour)

    all_neighbours = local_neighbours  # Add local neighbours to list of all neighbours of the current index n

    # For each local neighbour, if it exists in indices, find it's immediate neighbours and add to list of all
    # neighbours
    # NOTE - THIS STEP IS POSSIBLE BECAUSE PYTHON PASSES VALUES BY REFERENCE(THE REFERENCE TO indices IS PASSED TO THE
    # FUNCTION THAT IS CALLED, AND NOT THE ACTUAL VALUES INSIDE indices). THUS WHEN WE PASS indices IN RECURSIVE
    # CALLS, IT IS UPDATED AT THE MEMORY LOCATION (UPDATING OVER ENTIRE CHAIN OF RECURSION)  TO REMOVE ELEMENTS AND
    # PREVENT BACKWARD PROPAGATION  (THUS CAUSING AN INFINITE LOOP).
    for neighbour in local_neighbours:
        if neighbour in indices:
            gen_2_neighbours = extract_neighbours(indices, neighbour)
            all_neighbours += gen_2_neighbours
            indices.remove(neighbour)

    # Return the list of all neighbours of n
    return all_neighbours


# Test
print(segment_by_neighbours([[0, 1, 1, 0,],
                              [1, 1, 0, 0,],
                              [0, 0, 0, 1]]))  # Output -> [[[0, 0]], [[0, 3], [1, 3]], [[1, 2], [2, 2]], [[2, 0], [2, 1]], [[0, 1], [1, 1], [0, 2]], [[1, 0]], [[2, 3]]]
