"""
2. The image you get is known to have potentially many distinct rectangles of 0s on a background of 1's. Write a function that takes in the image and returns the coordinates of all the 0 rectangles -- top-left and bottom-right; or top-left, width and height.

image1 = [
[0, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 1, 1],
[0, 1, 1, 0, 0, 0, 1],
[1, 0, 1, 0, 0, 0, 1],
[1, 0, 1, 1, 1, 1, 1],
[1, 0, 1, 0, 0, 1, 1],
[1, 1, 1, 0, 0, 1, 1],
[1, 1, 1, 1, 1, 1, 0],
]

Sample output variations (only one is necessary):

findRectangles(image1) =>
// (using top-left-row-column and bottom-right):
[
[[0,0],[0,0]],
[[2,0],[2,0]],
[[2,3],[3,5]],
[[3,1],[5,1]],
[[5,3],[6,4]],
[[7,6],[7,6]],
]
"""

######################################################################################################
"""
The following code works to find all rectangles formed by 0s in an input matrix in the following steps -
1. Iterate over each element in the matrix.
2. In an element is 0, mark it as a starting point of the rectangle, continue checking vertically below it until a 1 is 
found.
3. From the index at which a 1 was found vertically, check horizontally until a 1 is found. 
4. Mark the point at which a 1 was found vertically as the end of the rectangle.
5. Append the start and end points to a list maintaining all rectangles in the image.
6. For every rectangle, store the list of all indices within that rectangle in an auxiliary list, to ensure we don't 
consider subsets of a rectangle as a new rectangle
"""
######################################################################################################


def find_rectangles(img):
    """
    Method to find rectangles of 0s in input image
    :param img: input nxn image
    :return: list of top-left-row-column and bottom-right coordinates of 0-rectangles in the image
    """
    rectangles = []  # Initialize a list to store coordinates of all rectangles
    points_in_rectangles = []  # Initialize a list to store points already covered in a rectangle to avoid considering subsets as new rectangles

    # Get image width and height for iterating
    img_height = len(img)
    img_width = len(img[0])

    # Iterate over each point in the image
    for i in range(img_height):
        for j in range(img_width):

            # If a point is 0, and it is not already discovered in an existing rectangle, consider it as the starting
            # point of a new rectangle
            if img[i][j] == 0 and ([i, j] not in points_in_rectangles):
                rectangle_start_point = [i, j]
                points_in_rectangles.append(rectangle_start_point)  # Add the point to discovered list

                n = 1
                # check vertically until a 1 is found, or the last row is reached
                while n < (img_height - i - 1) and img[i + n][j] == 0:
                    points_in_rectangles.append([i + n, j])
                    n += 1
                n -= 1  # decrement n to the last row where a 0 was found
                m = 1
                # Check horizontally until a 1 is found or the last column is reached
                while m < (img_width - j - 1) and img[i + n][j + m] == 0:
                    # Add all points within the rectangle to the discovered list
                    for px in range(n + 1):
                        points_in_rectangles.append([i + px, j + m])
                    m += 1
                m -= 1  # Decrement m to the last column where a 0 was found

                # the current index at i+n, j+m is the end of the reactangle
                rectangles.append([rectangle_start_point, [i + n, j + m]])
    return rectangles


# Tests
print(find_rectangles([
    [0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [0, 1, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 0, 1, 1],
    [1, 1, 1, 0, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 0],
]))  # [[[0, 0], [0, 0]], [[2, 0], [2, 0]], [[2, 3], [3, 5]], [[3, 1], [5, 1]], [[5, 3], [6, 4]], [[7, 6], [7, 6]]]

print(find_rectangles(
    [
        [0],
    ]
))  # [[[0, 0], [0, 0]]]

