"""
1. You are a developer for a university. Your current project is to develop a system for students to find courses they share with friends. The university has a system for querying courses students are enrolled in, returned as a list of (ID, course) pairs.

Write a function that takes in a list of (student ID number, course name) pairs and returns, for every pair of students, a list of all courses they share.

Sample Input:

student_course_pairs_1 = [
["58", "Linear Algebra"],
["94", "Art History"],
["94", "Operating Systems"],
["17", "Software Design"],
["58", "Mechanics"],
["58", "Economics"],
["17", "Linear Algebra"],
["17", "Political Science"],
["94", "Economics"],
["25", "Economics"],
["58", "Software Design"],
]

Sample Output (pseudocode, in any order):

find_pairs(student_course_pairs_1) =>
{
[58, 17]: ["Software Design", "Linear Algebra"]
[58, 94]: ["Economics"]
[58, 25]: ["Economics"]
[94, 25]: ["Economics"]
[17, 94]: []
[17, 25]: []
}
"""

######################################################################################################
"""
The following code aims to find common courses across student pairs by first storing each student's courses(as a set) 
in a dictionary, and then taking a pairwise intersection of all students. 
"""
######################################################################################################


def find_pairs(student_course_pairs):
    """
    Method to find common courses across student pairs
    :param student_course_pairs: input list containing student-course pairs
    :return: dictionary containing student pairs and their common courses
    """
    student_course_dict = dict()  # Initialize dict to store each student's courses in a set
    for student, course in student_course_pairs:
        student_course_dict.setdefault(student, set()).add(course)  # For each student, create an entry in the dictionary, and add the current course to that entry

    unique_students = list(student_course_dict.keys())  # Get a list of unique student Ids
    student_course_pair_intersection = dict()  # Initialize a dictionary to store the student-pair courses

    # Iterate over all unique student ids, and check their courses with students after them in the list
    for i in range(len(unique_students)):
        for j in range(i+1, len(unique_students)):
            # Get course set of each student
            student_1 = unique_students[i]
            student_2 = unique_students[j]
            key = "[" + student_1 + " , " + student_2 + "]"  # Prepare dictionary key using student ids

            # Find the intersection (common) of courses of both students and add to outut dictionary
            val = list(student_course_dict[student_1].intersection(student_course_dict[student_2]))
            student_course_pair_intersection[key] = val

    return student_course_pair_intersection


# Test
print(find_pairs([
    ["58", "Linear Algebra"],
    ["94", "Art History"],
    ["94", "Operating Systems"],
    ["17", "Software Design"],
    ["58", "Mechanics"],
    ["58", "Economics"],
    ["17", "Linear Algebra"],
    ["17", "Political Science"],
    ["94", "Economics"],
    ["25", "Economics"],
    ["58", "Software Design"],
]))

# Output - {'[58 , 94]': ['Economics'], '[58 , 17]': ['Software Design', 'Linear Algebra'], '[58 , 25]': ['Economics'], '[94 , 17]': [], '[94 , 25]': ['Economics'], '[17 , 25]': []}
