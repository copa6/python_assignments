# 3. Given an array, rotate the array to the right by k steps, where k is non-negative.
#
# Follow up:
#
# Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
# Could you do it in-place with O(1) extra space?
#
#
# Example 1:
#
# Input: nums = [1,2,3,4,5,6,7], k = 3
# Output: [5,6,7,1,2,3,4]
#
# Input: nums = [-1,-100,3,99], k = 2
# Output: [3,99,-1,-100]


def edge_case_k_greater_than_len(array, k):
    """
    Helper function to handle edge case when k is greater than the number of elements in the array.
    If k is more than the length of array, depending on the value of k, we either perform rotations on the same array
    or the reverse array
    :param array: input array
    :param k: k
    :return: updated array and k
    """
    num_full_rotations = k//len(array)  # Divide k by length of array to check how many full rotations of the array are needed
    k = k % len(array)  # Remainder of division gives us the rotations to be applied after full rotations
    if num_full_rotations % 2 == 1: # If number of full rotations is odd, reverse the array we operate on
        array = array[::-1]
    return array, k


def rotate_array_1(array, k):
    """
    Method 1 - Rotate k elements of the input array.
    Works by adding elements from the end of input array, to index 0 of new array for k steps.
    After k steps, appends remaining elements of input array to end of output array
    :param array: input array
    :param k: number of rotations
    :return: rotated array
    """
    # Edge case handler
    if k > len(array):
        array, k = edge_case_k_greater_than_len(array, k)

    output_array = []  # Initialize output array

    # Rotate k elements from the end
    for _ in range(k):
        output_array.insert(0, array[-1])  # Insert last element of the input array to 0 index of output array
        del array[-1]  # Delete last element of input array

    # Append remaining elements to the output array and return
    for elem in array:
        output_array.append(elem)
    return output_array


def rotate_array_2(array, k):
    """
    Method 3 - Rotate k elements of the input array. Works in O(1) space complexity
    Works by moving elements from the end of input array, to index 0 of the same array for k steps.
    :param array: input array
    :param k: number of rotations
    :return: rotated array
    """
    # Edge case handler
    if k > len(array):
        array, k = edge_case_k_greater_than_len(array, k)

    # Iteartively move elements from end of array to the beginning
    for _ in range(k):
        array.insert(0, array[-1])  # Copy element from end of array to index 0 of the array
        del array[-1]  # delete last element of the array

    return array


def rotate_array_3(array, k):
    """
        Method 3 - Rotate k elements of the input array. Works in O(1) space complexity
        Works by slicing the last k elements of the array and adding them to the beginning of the array
        :param array: input array
        :param k: number of rotations
        :return: rotated array
    """
    # Edge case handler
    if k > len(array):
        array, k = edge_case_k_greater_than_len(array, k)

    rev_idx = len(array)-k  # Calculate the number of elements to move from front to back

    # Concatenate last k elements and remaining (length-k) elements and return
    return array[-k:]+array[:rev_idx]


# Running the functions

# Test case 1
nums = [1,2,3,4,5,6,7]
k = 3
print(rotate_array_1(nums.copy(), k))  # output -> [5,6,7,1,2,3,4]
print(rotate_array_2(nums.copy(), k))  # output -> [5,6,7,1,2,3,4]
print(rotate_array_3(nums.copy(), k))  # # output -> [5,6,7,1,2,3,4]


# test case 2
nums = [-1,-100,3,99]
k = 2
print(rotate_array_1(nums.copy(), k))  # output -> [3, 99, -1, -100]
print(rotate_array_2(nums.copy(), k))  # output -> [3, 99, -1, -100]
print(rotate_array_3(nums.copy(), k))  # # output -> [3, 99, -1, -100]

# NOTE - WE USE .copy() WHILE CALLING OUR FUNCTIONS AS WE ARE UPDATING THE ARRAYS IN-PLACE WITHIN THE FUNCTION
# THIS UPDATED THE ARRAY VALUES AND WILL LEAD TO ERROR WHEN TRYING TO ACCESS THE SAME ARRAY IN NEXT FUNCTION
