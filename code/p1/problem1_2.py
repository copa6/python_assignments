# 2. Given an integer, write an algorithm to convert it to hexadecimal. For negative integer, two’s complement method is used.
#
# Note:
#
# All letters in hexadecimal (a-f) must be in lowercase.
# The hexadecimal string must not contain extra leading 0s. If the number is zero, it is represented by a single zero character '0'; otherwise, the first character in the hexadecimal string will not be the zero character.
# The given number is guaranteed to fit within the range of a 32-bit signed integer.
# You must not use any method provided by the library which converts/formats the number to hex directly.
# Example 1:
#
# Input:
# 26
#
# Output:
# "1a"
# Example 2:
#
# Input:
# -1
#
# Output:
# "ffffffff"

########################################################################################################
"""
The solution works in the following steps -
0. Create a list mapping each hexadecimal digit to the list's index.
1. We check if the number is 0, and return 0 if true.
2. We check if the integer is a negative number, and calculate it's two's complement using the relation:
            num + two's complement = 2^N, where N is the bit range (32 in our case)
3. Create an empty list to store characters of hexadecimal digit.
4. If the number is greater than 16, the remainder, after dividing by 16, gives us the first hexadecimal digit. We use
the list created in step 0, to get the hex code of the digit and append to the output list. For example, decimal number
26, after division with 16, leaves the remainder 10 -> We append 'a' to output list.
5. Update the number to the whole part of division by 16. For example, we update 26 -> int(26/16) = 2
6. Repeat step 4 and 5, until the number is lower than 16, and append the last digit using step 4.
7. Then output list contains hexadecimal digits in reverse order. So we concatenate it's elements in reverse, to get
the hexadecimal digit and return.
"""
########################################################################################################

# list containing hexadecimal characters at their respective decimal indices
hex_map = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f"]


def convert_to_hex(integer_num):
    """
    Function to convert integer input to hexadecimal output
    :param integer_num: integer number
    :return: hexadecimal equivalent of the input integer
    """

    hex_digits = []  # Initialize empty list to store hex digits
    if integer_num < 0:
        # Extract 2's complement of negative integer
        num = 2**32 + integer_num   # 2's complement of a = 2^N - a
    elif integer_num == 0:
        # return 0 for input 0
        return "0"
    else:
        num = integer_num

    # Extract hexadecimal digits from the number
    while num > 0:
        # If number is less than 16, append equivalent hexadecimal digit and stop iterations
        if num < 16:
            hex_digits.append(str(hex_map[num]))  # Append hexadecimal digit from reference list
            num = 0
        else:
            rem = num % 16  # Get the remainder after division by 16 -> This gives us the hex digit at nth position
            num = num // 16  # Divide by 16, for next iteration
            hex_digits.append(str(hex_map[rem]))  # Append hexadecimal digit from reference list

    # Join all elements of the output list into a string and return.
    # [::-1] is used to reverse the order. It indicates that we wish to go over all elements of the list starting from
    # the index 0 moving -1 step at a time.
    return "".join(hex_digits[::-1])


print(convert_to_hex(26))  # Test with input 26 -> output "1a"
print(convert_to_hex(-1))  # Test with input -1 -> output "ffffffff"
