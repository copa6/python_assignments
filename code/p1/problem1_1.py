# The functions in the following file solve the problem -
# Write a function that, given a string consisting of a word or words made up
# of letters from the word 'facebook', returns an integer with the number of
# stickers I will need to buy.
#
# get_num_stickers('coffee kebab') -> 3
# get_num_stickers('book') -> 1
# get_num_stickers('ffacebook') -> 2
#
# You can assume the input you are passed is valid, that is, does not contain
# any non-'facebook' letters, and the only potential non-letter characters
# in the string are spaces.

########################################################################################################
# The code works in the following steps -
# 1. Lower case the input word, to ensure that we are always working with text in same case.
# 2. Get a dictionary of count of individual characters in the input string(using get_alpha_counts method) -
# "coffee" -> {'c':1, 'o':1, 'f':2, 'e':2}
# 3. Get a dictionary of count of each character in our base word - facebook.
# 4. Remove any occurence of space in the input word(as we don't need spaces for the sticker)
# 5. Loop through the dictionary of characters created in step 2 and subtract the number of character that can be taken
    # from one instance of base word in each iteration of the loop, for example -
    # for "coffee kebab", step 2 yeilds - {'c':1, 'o':1, 'f':2, 'e':3, 'k':1, 'b':2, 'a':1}, and
    # "facebook" yeilds - {'f':1, 'a':1, 'c':1, 'e':1, 'b':1, 'o':2, 'k':1}
    # In iteration 1, we can get use all the characters from our base word, leaving us with
    # {'c':0, 'o':0, 'f':1, 'e':2, 'k':0, 'b':1, 'a':0}. We still need 2 'e' and an 'f' and 'b'
    # In iteration 2, we use one of each from our base word, and are left with {'c':0, 'o':0, 'f':0, 'e':1, 'k':0, 'b':0, 'a':0}
    # We go for another iteration to get the remaining occurence of e, and that leaves us with -
    # {'c':0, 'o':0, 'f':0, 'e':0, 'k':0, 'b':0, 'a':0}
# 6. As there are no more characters remaining in our string dictionary, we can stop the iterations.
# 7. The total number of loops/iterations we had to run, gives us the count of stickers we will need to complete our input string

########################################################################################################

BASE_WORD = "facebook"  # Define the base word


def get_alpha_counts(word):
    """
    function to get the count of each alphabet in a string, as a dictionary
    :param word: input string for which cound of characters is needed
    :return: dictionary of characters and their count in word

    NOTE - This functionality can be achieved by using an internal library collections.Counter()
    """
    alpha_counts_dict = {}  # intialize empty dictionary to store output

    # iterate through each character in the word string, and if character exists in the output dictionary,
    # add 1 to the count, else create a new entry in output dictionary with count=1
    for w in word:
        if w in alpha_counts_dict:
            alpha_counts_dict[w] += 1
        else:
            alpha_counts_dict[w] = 1
    return alpha_counts_dict


def get_num_stickers(input_string):
    input_string_lower = input_string.lower()  # lower case the input string

    # Get character count dictionary for input string and base word
    base_alpha_counts = get_alpha_counts(BASE_WORD)
    input_alpha_counts = get_alpha_counts(input_string_lower)

    # If there is space in input string but no space in the base word, remove space from character count dictionary
    if " " in input_alpha_counts and " " not in base_alpha_counts:
        input_alpha_counts.pop(" ")

    # Set count of iterations to 0
    i = 0
    input_words_remaining = True  # stop condition - When all characters have been extracted from stickers, stop the loop
    while input_words_remaining:
        i += 1  # increment loop counter
        input_words_remaining = False  # Set stop condition, which is updated within the loop

        for w in input_alpha_counts.keys():
            # For each character in the input string character count dictionary, we get the count of remaining
            # occurrences of it by subtracting from number of instances of same character, we can extract from the base word
            w_remaining = max(0, input_alpha_counts[w] - base_alpha_counts[w])  # lower bound to 0
            input_alpha_counts[w] = w_remaining  # Update the count of remaining instance of that character needed

            # If there is any character which is remaining to be extracted, we need another iteration,
            # and set the loop stop condition to continue for another iteration
            if w_remaining > 0:
                input_words_remaining = True

    # Return the total number of loops it took to extract all character in input string
    return i


get_num_stickers("ffacebook")
