"""
3. Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"
Note:

The length of both num1 and num2 is < 110.
Both num1 and num2 contain only digits 0-9.
Both num1 and num2 do not contain any leading zero, except the number 0 itself.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
"""

######################################################################################################
"""
The following code first converts string inputs to integer by iterating through each digit in the string
 - Then each digit is multiplied by 10^(position of digit) and summed, to get the integer.
 - The two numbers are multiplied as integers
 - The integer is converted back to string by iteratively taking remainder when dividing by 10, to 
   extract the digit followed by floored division by 10.
"""
######################################################################################################

# Maintain a map to convert decimal digits represented as string, to integer
integer_map = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9
}

reverse_integer_map = {v:k for k, v in integer_map.items()}  # Reverse the mapping to get integer to string map


def convert_str_to_int(s):
    """
    Helper function to convert a string to integer
    :param s: string to convert
    :return: integer form of the string
    """
    s_digits_reversed = list(s.strip())[::-1]  # Reverse the string to get digits in the order ones,tens,thousands... And convert to list such that each character is an element of the list
    num = 0  # Initialize number to 0
    sign = 1
    for i, digit in enumerate(s_digits_reversed):  # Use enumerate to iterate over list element and index at the same time
        # Extract sign from string, if present
        if digit == "-":
            sign = -1
        elif digit == "+":
            sign = 1
        else:
            num += integer_map[digit]*(10**i)  # For each character in string, get the appropriate integer number from map, multiply by 10^i, and add to the number

    # Multiply by 1/-1 (sign) to preserve sign
    return num*sign


def convert_int_to_str(num):
    """
    Helper function to convert an integer number to string representation
    :param num: integer number
    :return: string representation of the integer number
    """
    str_num = []  # Initialize empty list to store digits as string
    sign = "-" if num<0 else "" # If number is less than zero, preserve sign as - else no sign

    num = abs(num) # Convert the number to absolute value for conversion to string
    while num > 0:
        rem = num % 10  # Modulo division by 10 to extract last digit
        str_num.append(reverse_integer_map[rem])  # Get string representation of the digit using the reverse map
        num = num//10  # Floored division by 10 for next iteration

    # Append sign to the end of list
    str_num.append(sign)

    # Join the digits in the list in reverse order as list elements are ordered as ones, tens, thousands ...
    return "".join(str_num[::-1])


def multiply_string_integers(num1, num2):
    """
    Method to multiply two numbers represented as string
    :param num1: string representation of number 1
    :param num2: string representation of number 2
    :return: result of multiplying num1 and num2, returned as string
    """
    num1_int = convert_str_to_int(num1)  # Convert num1 to integer representation
    num2_int = convert_str_to_int(num2)  # Convert num2 to integer representation
    prod = num1_int*num2_int  # Multiply the two numbers
    return convert_int_to_str(prod)  # Convert back to string and return


# Test cases
print(multiply_string_integers("2", "3"))  # "6"
print(multiply_string_integers("123", "456"))  # "56088"

