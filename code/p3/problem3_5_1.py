"""
5. Design a Tic-tac-toe game that is played between two players on a n x n grid.

You may assume the following rules:

A move is guaranteed to be valid and is placed on an empty block.
Once a winning condition is reached, no more moves is allowed.
A player who succeeds in placing n of their marks in a horizontal, vertical, or diagonal row wins the game.
"""

######################################################################################################
"""
Tic Tac Toe game implementation in python - 
1. Initialize a nxn grid as a list of lists
2. For each move, add appropriate element (X/O) to index (x,y) in the grid
3. After each move, check if the move leads to a win, by - 
    - Check if there is any row where all elements are the same 
    - Check if there are any columns where all elements are same - This is done by transposing the grid and checking across rows
    - Check if the main diagonal elements are all same - Done by checking (1,1), (2,2), (3,3)...
    - Check if reverse diagonal elements are all same - Done by checking (1,last_column), (2,last_column-2)... 
"""
######################################################################################################


class TicTacToe:
    """ Tic Tac Toe implementation in python"""
    def __init__(self, n):
        """
        Initialize an empty nxn grid for the game
        :param n: size of grid
        """
        self.n = n  # Store n as a class variable to be used while computing winner
        row = [" " for _ in range(n)]  # Create a row of n elements
        self.grid = [row.copy() for _ in range(n)]  # Create grid by copying the row n times
        self.players = ["X", "O"]  # Store a map of user symbols
        self.print_grid()  # print grid before the start of the game

    def print_grid(self):
        """
        Helper method to print the grid after each move
        """
        for row in self.grid:
            row_line = "|"+ '|'.join(row) + "|"
            print(row_line)
        print("-"*20)

    def move(self, x, y, player):
        """
        Method to register a move
        :param x: x coordinate of the move
        :param y: y coordinate of the move
        :param player: player id 1/2
        :return: if the move leads to a win, return player id, else return 0
        """
        player_sign = self.players[player-1]  # Get the player sign
        print("Player {0} moved at {1}, {2}".format(player, x, y))

        # Check if there already exists an element at location x,y and flag as illegal move
        if self.grid[x][y] != " ":
            print("Illegal move")
        else:
            self.grid[x][y] = player_sign  # Add player sign (X/O) at the index
            self.print_grid()  # Print the grid
            return self.check_win_naive()  # Return win status

    def check_win_naive(self):
        """
        Method to check if the grid has a winning combination
        :return: 0 if no win, else player id of the winner
        """
        # check horizontal match
        winner = self.check_row_win(self.grid)
        if winner is None:
            # check vertical match
            transposed_grid = list(zip(*self.grid))  # Unwrap the grid, and use zip to combine along column and transpose the list
            winner = self.check_row_win(transposed_grid)  # Check if any row has winning combination in transposed grid
            if winner is None:
                # check diagonal match
                # Use set to get a list of unique elements across main diagonal. Main diagonal elements are located at i,i in range 0 to n
                row_diag_1 = list(set([self.grid[i][i] for i in range(self.n)]))

                # If the diagonal is non empty and contains only one unique element, mark it as the winner
                if len(row_diag_1) == 1 and row_diag_1[0] != " ":
                    winner = row_diag_1[0]
                else:
                    # Check across other diagonal
                    # Use set to get a list of unique elements across reverse diagonal.
                    # Reverse diagonal elements are located at i,n-i-1 in range 0 to n. (0,2), (1,1), (2,0)
                    row_diag_2 = list(set([self.grid[i][self.n-i-1] for i in range(self.n)]))

                    # If the reverse diagonal is non empty and contains only one unique element, mark it as the winner
                    if len(row_diag_2) == 1 and row_diag_2[0] != " ":
                        winner = row_diag_2[0]

        # If any of the strategies found a winner, extract the winning id and return
        if winner is not None:
            winning_player = self.players.index(winner) + 1
            return winning_player
        else:
            return 0

    def check_row_win(self, grid):
        """
        Helper method to check if any row in the grid contains winning strategy
        :param grid: Grid to check for winning combination across rows
        :return: winner sign if there is a winner, else None
        """
        for row in grid:
            # If the row is non empty and contains only one unique element, mark it as the winner
            if len(set(row)) == 1 and " " not in row:
                return row[0]
        return None


# # Test
toe = TicTacToe(3)

print(toe.move(0, 0, 1))
print(toe.move(0, 2, 2))
print(toe.move(2, 2, 1))
print(toe.move(1, 1, 2))
print(toe.move(2, 0, 1))
print(toe.move(1, 0, 2))
print(toe.move(2, 1, 1))

# Test to check winner across non-main diagonal
# toe = TicTacToe(3)
# print(toe.move(0, 0, 1))
# print(toe.move(0, 2, 2))
# print(toe.move(2, 2, 1))
# print(toe.move(1, 1, 2))
# print(toe.move(1, 0, 1))
# print(toe.move(2, 0, 2))
