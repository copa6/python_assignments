"""
1.Reverse a singly linked list.

Example:

Input: 1->2->3->4->5->NULL
Output: 5->4->3->2->1->NULL
Follow up:

A linked list can be reversed either iteratively or recursively. Could you implement both?

Please think of any other list manipulation that comes to your mind that can be performed with a list.
"""

######################################################################################################
"""
The code works by first creating a class to create and traverse linked list and then reverse it using the following steps - 
I. Iterative method method - 
    1. Create a local copy of the linked list to manipulate within function
    2. Start with the head of the linked list, marking it's previous node as empty.
    3. For each node, reverse it's immediate neighbours - 
        - Get reference to next node in forward linked list.
        - Mark the previous node as the current node's next.
        - Move to the reference to next node and repeat.
    4. Step 3 is repeated for all nodes in the Linked List.

II. Recursive method - 
   1. Create a local copy of the linked list to manipulate within function
   2. If there are no elements in the linked list, return it as is.
   3. If there are elements in the linked list - 
        - Start with first element, and invert mapping from current -> next node, to current -> previous node.
        - Move to the next element and repeat the process until there are no elements in the LL 
        
"""
######################################################################################################

from copy import deepcopy


class Node:
    """
    Class to define node of a linked list
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    """
    Class to implement a linked list in python
    """
    def __init__(self):
        """
        Initialize the linked list with None at it's head
        """
        self.head = None

    def create(self, list_data):
        """
        Creates a linked list from input list of elements
        :param list_data: data to be put into linked list
        :return: None
        """
        for data in list_data[::-1]:
            self.push(data)

    def push(self, data):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param data: single element to be added to the linked list
        :return: None
        """
        node = Node(data)  # Create a new node instance
        node.next = self.head  # Mark the node to be current head's previous node
        self.head = node  # Update reference of head to current node

    def show(self):
        """
        Helper method to print the linked list instance
        :return:
        """
        node = self.head
        print_output = []
        while node:
            print_output.append(str(node.data))
            node = node.next
        print("->".join(print_output))


def reverse_ll_iterative(linked_list):
    """
    Method to iteratively reverse a linked list iteratively.
    :param linked_list: Input linked list
    :return: reversed linked list
    """
    previous_node = None  # Mark previous instance as None. This will be used as the tail of reversed LL
    local_ll = deepcopy(linked_list)  # deep copy the linked list instance to avoid overriding the original LL
    current_node = local_ll.head  # Start with the head of the LL, and reverse .next mapping one node at a time

    # Iterate till the tail (None) of the LL
    while current_node:
        next_node = current_node.next  # Get a reference to the next element in the LL
        current_node.next = previous_node  # Reverse current node's next attribute to point to the previous node

        # Move one step to the right for next iteration
        previous_node = current_node
        current_node = next_node

    local_ll.head = previous_node  # Mark the last node as the head of the reversed linked list
    return local_ll


def recursive_reverse_helper(ll, current_node, previous_node):
    """
    Helper function to reverse a set of elements in LL
    :param ll: linked list to reverse
    :param current_node: reference to the current node
    :param previous_node: reference to the previous node
    :return: None
    """

    # If current node is last node of the LL, mark it as the head, and previous node as it's next and return
    if current_node.next is None:
        ll.head = current_node
        current_node.next = previous_node
        return

    # Update node reference to next set of nodes
    next_node = current_node.next
    current_node.next = previous_node

    # Recursive  call to reverse the next set of nodes
    recursive_reverse_helper(ll, next_node, current_node)


def reverse_ll_recursive(linked_list):
    """
    Main function to reverse linked list recursively
    :param linked_list: LL to reverse
    :return: reversed LL
    """
    local_ll = deepcopy(linked_list)  # deep copy the linked list instance to avoid overriding the original LL
    if local_ll.head is None:
        return local_ll  # If there are no elements in LL, return as is
    else:
        recursive_reverse_helper(local_ll, local_ll.head, None)  # Call helper function to reverse head and None
    return local_ll


# Test
ll = LinkedList()  # Create a new linked list instance
ll.create([1, 2, 3, 4, 5])  # Add elements to the LL
ll.show()  # print the linked list: 1->2->3->4->5

reverse_ll_iterative(ll).show()  # Reverse the LL iteratively: 5->4->3->2->1

reverse_ll_recursive(ll).show()  # Reverse the LL recursively: 5->4->3->2->1
