"""
5. Design a Tic-tac-toe game that is played between two players on a n x n grid.

You may assume the following rules:

A move is guaranteed to be valid and is placed on an empty block.
Once a winning condition is reached, no more moves is allowed.
A player who succeeds in placing n of their marks in a horizontal, vertical, or diagonal row wins the game.
"""

######################################################################################################
"""
Tic Tac Toe game implementation in python - 
1. Initialize a nxn grid as a list of lists
2. For each move, add appropriate element (X/O) to index (x,y) in the grid
3. After each move, check if the move leads to a win, by iterating through the rows of the grid-
    - Count number of elements of the same sign as player sign across rows, columns and the two diagonals  
4. If any of the count is equal to grid size n, it indicates a winning entry.
"""
######################################################################################################


class TicTacToe:
    def __init__(self, n):
        self.n = n  # Store n as a class variable to be used while computing winner
        row = [" " for _ in range(n)]  # Create a row of n elements
        self.grid = [row.copy() for _ in range(n)]  # Create grid by copying the row n times
        self.players = ["X", "O"]  # Store a map of user symbols
        self.print_grid()  # print grid before the start of the game
        self.num_moves = 0

    def print_grid(self):
        """
        Helper method to print the grid after each move
        """
        for row in self.grid:
            row_line = "|"+ '|'.join(row) + "|"
            print(row_line)
        print("-"*20)

    def move(self, x, y, player):
        """
        Method to register a move
        :param x: x coordinate of the move
        :param y: y coordinate of the move
        :param player: player id 1/2
        :return: if the move leads to a win, return player id, else return 0
        """
        player_sign = self.players[player-1]   # Get the player sign
        print("Player {0} moved at {1}, {2}".format(player, x, y))

        # Check if there already exists an element at location x,y and flag as illegal move
        if self.grid[x][y] != " ":
            print("Illegal move")
        else:
            self.num_moves += 1  # add a move to class counter
            self.grid[x][y] = player_sign  # Add player sign (X/O) at the index
            self.print_grid()

            # If fewer than n-1 moves are made, there can be no winner. So check only after atleast n-1 moves are done
            if self.num_moves > self.n:
                is_winner = self.check_win_low_complexity(x, y, player_sign)
                winner = player if is_winner else 0  # If the current move leads to a win, mark the current plauyer as winner
                return winner
            else:
                return 0

    def check_win_low_complexity(self, x, y, player_sign):
        """
        Helper function to check if a move leads to a win
        :param x: x coordinate of the move
        :param y: y coordinate of the move
        :param player_sign: sign of the player making the move
        :return: boolean indicating if the current move leads to a win for the player
        """
        horizontal = 0  # count to track horizontal entries of player
        vertical = 0  # count to track vertical entries of player
        diag = 0  # count to track diagonal entries of player
        rev_diag = 0  # count to track reverse-diagonal entries of player

        for i in range(self.n):
            # If the entry in same column at different rows, is the same as player sign, add 1 to vertical count
            if self.grid[x][i] == player_sign:
                vertical += 1

            # If the entry in same row at different columns, is the same as player sign, add 1 to horizontal count
            if self.grid[i][y] == player_sign:
                horizontal += 1

            # If the entry in diagonal element is same as player sign, add 1 to diagonal count
            if self.grid[i][i] == player_sign:
                diag += 1

            # If the entry in reverse diagonal element is same as player sign, add 1 to reverse diagonal count
            if self.grid[i][self.n-i+-1] == player_sign:
                rev_diag += 1

        # If any of the above counts is equal to size of grid, that indicates that same element is present across the
        # row/column/diagonal of the grid, indicating a win
        if horizontal == self.n or (vertical==self.n) or (diag==self.n) or (rev_diag==self.n):
            return True
        else:
            return False



# # Test
# toe = TicTacToe(3)
#
# print(toe.move(0, 0, 1))
# print(toe.move(0, 2, 2))
# print(toe.move(2, 2, 1))
# print(toe.move(1, 1, 2))
# print(toe.move(2, 0, 1))
# print(toe.move(1, 0, 2))
# print(toe.move(2, 1, 1))


toe = TicTacToe(3)
print(toe.move(0, 0, 1))
print(toe.move(0, 2, 2))
print(toe.move(2, 2, 1))
print(toe.move(1, 1, 2))
print(toe.move(1, 0, 1))
print(toe.move(2, 0, 2))
