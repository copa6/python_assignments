"""
2. Given two strings s and t , write a function to determine if t is an anagram of s.

Example 1:

Input: s = "anagram", t = "nagaram"
Output: true
Example 2:

Input: s = "rat", t = "car"
Output: false
Note:
You may assume the string contains only lowercase alphabets.

Follow up:
What if the inputs contain unicode characters? How would you adapt your solution to such case?
"""

######################################################################################################
"""
To check whether two strings are anagrams of each other, we take the count of alphabets in each string and match whether 
both the strings have the same count of each alphabet. If true, then the two strings are anagrams, else not.
"""
######################################################################################################

# Use counter to get the alphabet count in each string
from collections import Counter


def check_anagrams(s, t):
    """
    Function to check if the two strings are anagrams.
    :param s: String 1
    :param t: String 2
    :return: boolean indicating if the two strings are anagrams
    """

    # If the length of the two strings is not same, we can say that the two strings are not anagrams and return a False
    if len(s) != len(t):
        return False

    s_alphabet_counts = Counter(s)  # Get count of all alphabets in s as a dictionary
    t_alphabet_counts = Counter(t)  # Get count of all alphabets in t as a dictionary

    # For each alphabet in s, check if count in s matches count in t, and return False is there is any mismatch
    for k,v in s_alphabet_counts.items():
        if t_alphabet_counts.get(k, -99) != v:
            return False
    return True


# Test cases
print(check_anagrams("anagram", "nagaram"))  # True
print(check_anagrams("rat", "car"))  # False
print(check_anagrams("rat", "tar"))  # True
print(check_anagrams("Россия", "Росяси"))  # With unicode characters -> True
print(check_anagrams("Россияgood", "Росясиbad"))  # With unicode characters -> False
