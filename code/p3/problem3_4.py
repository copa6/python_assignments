"""
4. Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.



Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
Example 2:

Input: strs = [""]
Output: [[""]]
Example 3:

Input: strs = ["a"]
Output: [["a"]]

"""

######################################################################################################
"""
The following code works to check anagrams by first sorting each string in the list alphabetically and storing in a 
new list - This ensures that all anagrams look the same in the sorted list.
Anagrams are extracted from the original list by putting all elements that have the same representation in the sorted 
list, into a common key in a dictionary. 
List of dictionary values gives the anagrams groups. 
 
"""
######################################################################################################


def group_anagrams_in_list(strings_list):
    """
    Function to find sets of anagrams in a list of strings
    :param strings_list: list of strings to check for anagrams
    :return: list of groups of anagrams
    """
    # Sort each string alphabetically and store in new list - This ensures that all anagrams are represented the same
    # way in new sorted_list
    sorted_list = ["".join(sorted(s)) for s in strings_list]
    anagrams_dict = {}  # Initialize dictionary to store anagram groups

    # Iterated through sorted list and group together elements that look the same in sorted_list
    for i, s in enumerate(sorted_list):
        anagrams_dict.setdefault(s, []).append(strings_list[i])  # setdefaults creates a new key in the dictionary with the default value, if it does not exist already

    grouped_anagrams = list(anagrams_dict.values())  # Return the values of dictionary put into a list
    return grouped_anagrams


# Test
print(group_anagrams_in_list(["eat","tea","tan","ate","nat","bat"]))  # Output: [['eat', 'tea', 'ate'], ['tan', 'nat'], ['bat']]
print(group_anagrams_in_list(["a"]))  # Output: [['a']]
print(group_anagrams_in_list([""]))  # Output: [['']]

