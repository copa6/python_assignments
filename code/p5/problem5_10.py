"""
10. Given two integer arrays A and B, return the maximum length of an subarray that appears in both arrays.

Example 1:

Input:
A: [1,2,3,2,1]
B: [3,2,1,4,7]
Output: 3
Explanation:
The repeated subarray with maximum length is [3, 2, 1].


Note:

1 <= len(A), len(B) <= 1000
0 <= A[i], B[i] < 100
"""


######################################################################################################
"""
The following code works to find the largest common subarray between two arrays in the following steps - 
1. Start from the first index of first array 
2. In the second array, starting with the first index, check if the element at index j matches the element from the 
first array.
3. If there is a match, check if subsequent elements of each array match and keep adding the matched array to a local 
common sub-array.
4. If the length of the common sub-array is larger than any sub array previously encountered, mark this as the largest 
sub-array and continue.
5. Return the length of the largest sub array.
"""
######################################################################################################


def find_max_length_subarray(arr1, arr2):
    """
    Method to find the length of the largest common sub-array between the two input arrays
    :param arr1: input array 1
    :param arr2: input array 2
    :return: length of the largest common sub-array between the two input arrays
    """
    arr1_length = len(arr1)  # Extract length of array 1 -> O(n)
    arr2_length = len(arr2)  # Extract length of array 2 -> O(m)

    longest_common_subarray = []  # Initialize a placeholder for the largest common sub-array

    # Iterate over each index of first array  -> O(n)
    for i in range(arr1_length):
        # Iterate over each index of second array -> O(m)
        for j in range(arr2_length):
            common_arr = []  # Initialize an empty array to hold the current common array -> O(1)

            # If current element of second array, matches with the current element of first array, add to common array
            # and check subsequent elements
            if arr1[i] == arr2[j]:
                common_arr.append(arr1[i])  # -> O(1)
                n = 1  # Initialize loop counter

                # Loop as long as subsequent elements of both array are same
                while arr1[i + n] == arr2[j + n]:  # -> O(n) or O(m) whichever is larger
                    common_arr.append(arr1[i + n])  # Append to common array -> O(1)
                    n += 1  # Move to next element -> O(1)
                    if (n == arr1_length-i) or (n == arr2_length-i):  # If end of either array is reached, exit the loop
                        break

                # If length of current common subarray is greater than largest common subarray, update largest -> O(n)
                if len(common_arr) > len(longest_common_subarray):
                    longest_common_subarray = common_arr

                # Early exit condition to stop early if length of larget common subarray is greater then elements
                # remaining in second array. This is something that is not captured well by big-O notation, as that is
                # focused on the upper bound of running time.
                if len(longest_common_subarray) > arr2_length - j:
                    break

        # Early exit condition for first array
        if len(longest_common_subarray) > arr1_length - i:
            break
    print(longest_common_subarray)
    return len(longest_common_subarray)


"""
Total big-O complexity = O(n) * O(m) * O(n) or O(n) * O(m) * O(m) whichever is larger.
 As a convention we can say its O(n*m^2)
         
"""

# Test
print(find_max_length_subarray([1, 2, 3, 2, 1], [3, 2, 1, 4, 7]))  # 3
