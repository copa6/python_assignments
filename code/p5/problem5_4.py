"""
4. You will be supplied with two data files in CSV format .
The first file contains statistics about various dinosaurs. The second file contains additional data.
Given the following formula, speed = ((STRIDE_LENGTH / LEG_LENGTH) - 1) * SQRT(LEG_LENGTH * g)
Where g = 9.8 m/s^2 (gravitational constant)

Write a program to read in the data files from disk, it must then print the names of only the bipedal dinosaurs from fastest to slowest.
Do not print any other information.

$ cat dataset1.csv
NAME,LEG_LENGTH,DIET
Hadrosaurus,1.4,herbivore
Struthiomimus,0.72,omnivore
Velociraptor,1.8,carnivore
Triceratops,0.47,herbivore
Euoplocephalus,2.6,herbivore
Stegosaurus,1.50,herbivore
Tyrannosaurus Rex,6.5,carnivore

$ cat dataset2.csv
NAME,STRIDE_LENGTH,STANCE
Euoplocephalus,1.97,quadrupedal
Stegosaurus,1.70,quadrupedal
Tyrannosaurus Rex,4.76,bipedal
Hadrosaurus,1.3,bipedal
Deinonychus,1.11,bipedal
Struthiomimus,1.24,bipedal
Velociraptorr,2.62,bipedal
"""

######################################################################################################
"""
The following code works in the following steps - 
1. Load the csv data
2. Join both data files using the "NAME" column
3. Filter only bipedals from the data
4. For filtered data, calculate speed for each row(dinosaur)
5. Sort the data by speed, in descending order
6. print names from the sorted data
"""
######################################################################################################


import pandas as pd  # Use pandas for data manipulation
import math  # For square-root functionality

g = 9.8  # Constant


def get_bipedal_dinosaurs(file1, file2):
    """
    Code to read in the data files from disk, and print the names of only the bipedal dinosaurs from fastest to slowest.
    :param file1: path to file containing data 1
    :param file2: path to file containing data 2
    :return: print the list of bipedal dinosaurs from fastest to slowest
    """

    # Read the data into dataframes
    file1_data = pd.read_csv(file1)
    file2_data = pd.read_csv(file2)
    all_data = pd.merge(file1_data, file2_data, on="NAME")  # Join both data using "Name" column

    bipedal_dinosaurs = all_data.loc[all_data["STANCE"] == "bipedal"].reset_index(drop=True)  # Filter only bipedals

    # For bipedals, calculate speed using the formula ((STRIDE_LENGTH / LEG_LENGTH) - 1) * SQRT(LEG_LENGTH * g)
    bipedal_dinosaurs["speed"] = [((stride / leg_len) - 1) * math.sqrt(leg_len * g) for stride, leg_len in
                                  zip(bipedal_dinosaurs["STRIDE_LENGTH"], bipedal_dinosaurs["LEG_LENGTH"])]

    # Sort the data in descending order of speed
    fastest_bipedals = bipedal_dinosaurs.sort_values("speed", axis=0, ascending=False)["NAME"]

    # Print names of bipedal dinosaurs from fastest to slowest
    for dino in fastest_bipedals:
        print(dino)


get_bipedal_dinosaurs("dataset1.csv", "dataset2.csv")  # Struthiomimus \n Hadrosaurus \n Tyrannosaurus Rex
