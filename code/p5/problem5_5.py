"""
5. A sentence S is given, composed of words separated by spaces. Each word consists of lowercase and uppercase letters only.

We would like to convert the sentence to "Goat Latin" (a made-up language similar to Pig Latin.)

The rules of Goat Latin are as follows:

If a word begins with a vowel (a, e, i, o, or u), append "ma" to the end of the word.
For example, the word 'apple' becomes 'applema'.

If a word begins with a consonant (i.e. not a vowel), remove the first letter and append it to the end, then add "ma".
For example, the word "goat" becomes "oatgma".

Add one letter 'a' to the end of each word per its word index in the sentence, starting with 1.
For example, the first word gets "a" added to the end, the second word gets "aa" added to the end and so on.
Return the final sentence representing the conversion from S to Goat Latin.
"""

######################################################################################################
"""
The following code converts a string to goat latin in 5 steps - 
1. Split the string at space and store individual words in a list
2. For each word, if it does not start with a vowel, move the first alphabet of the word to the last
3. Add "ma" to the end of the word
4. Add "a" equal to the index of the word, to the end of the word
5. Append word to output string
"""
######################################################################################################


def convert_to_goat_latin(s):
    """
    Code to convert input string to goat-latin
    :param s: input string
    :return: goat latin form of input string
    """
    s_words = s.split()  # Split the word at space and store words in a list
    output_words = []  # Initiliaze list for output

    # Convert each word in input to its goat latin equivalent
    for i, word in enumerate(s_words):
        # if word does not start with a vowel, move the first alphabet of the word to the last
        if word[0] not in ["a", "e", "i", "o", "u"]:
            word = word[1:] + word[0]

        # Add "ma" to the end of the word and Add "a" equal to the index of the word, to the end of the word
        output_words.append(word + "ma" + "a"*(i+1))

    # Join output words and return
    return " ".join(output_words)


# Test
print(convert_to_goat_latin("apple"))  # Output: applemaa
print(convert_to_goat_latin("goat"))  # Output: oatgmaa
print(convert_to_goat_latin("This is a goat"))  # Output: hisTmaa ismaaa amaaaa oatgmaaaaa
