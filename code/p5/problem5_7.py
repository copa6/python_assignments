"""
7. Design a data structure for History Set with following operations:

Add(element) : Adds an element to the Set and returns a SequenceId
Discard(element): Discards an element from the Set and returns a SequenceId
Member(sequenceId): Return the state of historySet at a given sequenceId
Example:
seq1 = add("a")
seq2 = add("b")
seq3 = add("c")
seq4 = discard("b")

member(seq3) = ("a", "b", "c")
member(seq1) = ("a")
member(seq4) = ("a", "c")
"""

######################################################################################################
"""
The following class implements a HistorySet as explained in the specifications. 
It uses teh following for overall implementation - 
1. Dictionary: To store the histories
2. Set: To store the elements
"""
######################################################################################################


class HistorySet:
    """
    Class to implement a history set
    """
    def __init__(self):
        """
        Initialize the class with empty histories and the set
        """
        self.history_set = dict()  # Initialize dictionary to store histories
        self.latest_set = set()  # Initialize a set to store the latest state
        self.curr_insert = 0  # Initialize a key to store sequence ids

    def update_history_and_return(self):
        """
        Helper method to store changes in history dictionary
        :return: sequence id for latest change
        """
        self.curr_insert += 1  # Update the sequence key to next number
        self.history_set[
            self.curr_insert] = self.latest_set.copy()  # Add a copy of the updated set to histories dictionary
        # return the insertion index as sequence id
        return self.curr_insert

    def add(self, element):
        """
        Method to add element to history set
        :param element: Element to be added
        :return: returns sequence id after adding
        """
        self.latest_set.add(element)  # Add element to the list
        seq_id = self.update_history_and_return()
        return seq_id

    def discard(self, element):
        """
        Method to discard element from the set
        :param element: element to be removed
        :return: sequence id after removing the element
        """
        self.latest_set.remove(element)  # removes element from the latest set
        seq_id = self.update_history_and_return()
        return seq_id

    def member(self, member_id):
        """
        Method to return the set for a sequence id
        :param member_id: sequence id
        :return: set corresponding to the input sequence id
        """
        return self.history_set[member_id]


# Test
history_set = HistorySet()
seq1 = history_set.add("a")
seq2 = history_set.add("b")
seq3 = history_set.add("c")
seq4 = history_set.discard("b")

print(history_set.member(seq3))  # Output = ("a", "b", "c")
print(history_set.member(seq1))  # Output = ("a")
print(history_set.member(seq4))  # Output = ("a", "c")
