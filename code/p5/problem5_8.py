"""
8. Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

Example 1:

Input: "1 + 1"
Output: 2
Example 2:

Input: " 2-1 + 2 "
Output: 3
Example 3:

Input: "(1+(4+5+2)-3)+(6+8)"
Output: 23
Note:
You may assume that the given expression is always valid.
Do not use the eval built-in library function.
"""

######################################################################################################
"""
The following code works as a string based calculator using the following logic - 
1. Replace all spaces in text.
2. Replace all cases in the code where there is a sign followed by one or more open parenthesis and another sign with an
equivalent sign - 
    a) For +(+ -> Replace with +
    b) For +(- -> Replace with -
    c) For -(- -> Replace with +
    d) For -(+ -> Replace with -
3. Replace all other parenthesis, and spaces.
4. Extract the first digit from index 0 if it is not a signed integer, else extract from index 0 & 1, and place in
output variable
5. Iterate over remaining elements, adding or subtracting the integer at index (i+1) from the output, based on sign at 
index i. 
"""
######################################################################################################

import re


def calculator(s):
    """
    Calculate the output of input expression string
    :param s: input string containing the expression to be evaluated
    :return: result of the expression
    """
    processed_s = s.replace(" ", "")  # Replace spaces  -> O(n)

    # Regexp based replace for sing-parenthesis combination
    processed_s = re.sub(r'\+\(+\+', '+', processed_s)  # -> O(1)
    processed_s = re.sub(r'\+\(+\-', '-', processed_s)  # -> O(1)
    processed_s = re.sub(r'\-\(+\-', '+', processed_s)  # -> O(1)

    # Replace other values
    processed_s = processed_s.strip().replace("(", "").replace(")", "").replace(" ", "")  # -> O(1)

    # Extract the first integer with/without sign
    if processed_s[0] == "-":
        output = int(processed_s[1]) * -1  # Multiply the first number with -1 if first element is a sign -> O(1)
        processed_s = processed_s[2:]  # Remove first two indices from string -> O(n)
    elif processed_s[0] == "+":
        output = int(processed_s[1])  # Multiply the first number with -1 if first element is a sign -> O(1)
        processed_s = processed_s[2:]  # Remove first two indices from string -> O(n)
    else:
        output = int(processed_s[0])
        processed_s = processed_s[1:]  # Remove first index from string -> O(n)

    # Process all other indices as (sign, integer) groups.
    # Based on sign at index i, add or subtract integer at index i+1
    for i in range(0, len(processed_s), 2):  # -> O(n)
        if processed_s[i] == "+":
            output += int(processed_s[i+1])
        else:
            output -= int(processed_s[i + 1])
    return output


"""
Total big O complexity  = Upper bound of sum of all other complexities
                        = O(n) + O(1) + O(1) + O(1) + O(1) + O(1) + O(n)....
                        = O(n) -> Indicating that the time complexity increases linearly with the size of the string
"""


# Test
print(calculator("1 + 1"))  # 2
print(calculator(" 2-1 + 2 "))  # 3
print(calculator("(1+(4+5+2)-3)+(6+8)"))  # 23
print(calculator("(1+((-4)+4+5+2)-3)+(6+8)"))  # 19

