"""
2. Split an array into two equal Sum subarrays

Last Updated: 09-05-2020
Given an array of integers greater than zero, find if it is possible to split it in two subarrays (without reordering the elements), such that the sum of the two subarrays is the same. Print the two subarrays.

Examples :

Input : Arr[] = { 1 , 2 , 3 , 4 , 5 , 5  }
Output :  { 1 2 3 4 }
          { 5 , 5 }

Input : Arr[] = { 4, 1, 2, 3 }
Output : {4 1}
         {2 3}

Input : Arr[] = { 4, 3, 2, 1}
Output : Not Possible
"""
######################################################################################################
"""
The following code finds equal sum subarrays in an array by iterating over all element of the array and at each step
checking whether the sum of elements up to the current element is equal to sum of all remaining elements. If this 
condition is met, it returns the two subarrays where the condition is met, else retruns "Not Possible"
"""
######################################################################################################


def find_equal_sum_subarrays(arr):
    """
    The following code finds equal sum sub arrays within an array by taking slice at each index of the array and
    checking if sum of elements in left slice is equal to sum of elements in the right slice
    :param arr: input array
    :return: equal sum subarrays or "Not Possible"
    """

    # For each index in array, check if sum of elements in left slice is equal to sum of elements in right slice
    # Return sub arrays on truth condition
    for i in range(len(arr)):
        if sum(arr[:i]) == sum(arr[i:]):
            return arr[:i], arr[i:]
    return "Not Possible"


# Test
print(find_equal_sum_subarrays([1, 2, 3, 4, 5, 5]))  # Output: ([1, 2, 3, 4], [5, 5])
print(find_equal_sum_subarrays([4, 1, 2, 3]))  # Output: ([4, 1], [2, 3])
print(find_equal_sum_subarrays([4, 3, 2, 1]))  # Output: Not Possible
