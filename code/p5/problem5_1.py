"""
1. You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

You need to do this in place.

Note that if you end up using an additional array, you will only receive partial score.

Example:

If the array is

[
    [1, 2],
    [3, 4]
]
Then the rotated array becomes:

[
    [3, 1],
    [4, 2]
]

"""

######################################################################################################
"""
Following code works to rotate an array in-place by cyclically moving elements to their right counter-part. 
The code starts with the outer rows/columns (outer circle) of the matrix, by moving element in the following order
Store element at [0, 0] in a temporary variable
[N, 0] -> [0, 0]
[N, N] -> [N, 0]
[0, N] -> [N, N]
temp -> [0, N]



This completes one round of replacements. 
Then the code moves to the next element of the outer row/column and moves elements in the following order
[0,1] -> [1, N]
[1, N] -> [N, N-1]
[N, N-1] -> [N-1, 0]
[N-1, 0] -> [0, 1]

Thus the code cyclically moves all element of the outer rows/columns, and moves one level deeper in the next iteration. 
Once the algorithm reaches the center of the array, all elements have been rotated.
"""
######################################################################################################


def rotate_array(arr):
    """
    The following code rotates an array in-place, by moving the elements cyclically until all elements have been moved
    :param arr: input array
    :return: rotated array
    """
    n = len(arr[0])  # Get the length of the array

    # Iterate till reaching the center of the array
    for i in range(n // 2):

        # Iterate over all elements in the current scope (circle)
        for j in range(i, n - i - 1):
            temp = arr[i][j]  # Store the first element is temp variable
            arr[i][j] = arr[n - 1 - j][i]  # Move element from left to top
            arr[n - 1 - j][i] = arr[n - 1 - i][n - 1 - j]  # Move element from bottom to left
            arr[n - 1 - i][n - 1 - j] = arr[j][n - 1 - i]  # Move element from right to bottom
            arr[j][n - 1 - i] = temp  # Move element from top(stored in temp) to right

    return arr


# Test
print(rotate_array([[1, 2], [3, 4]]))  # [[3, 1], [4, 2]]
