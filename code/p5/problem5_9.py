"""
9. A website domain like "discuss.leetcode.com" consists of various subdomains. At the top level, we have "com", at the next level, we have "leetcode.com", and at the lowest level, "discuss.leetcode.com". When we visit a domain like "discuss.leetcode.com", we will also visit the parent domains "leetcode.com" and "com" implicitly.

Now, call a "count-paired domain" to be a count (representing the number of visits this domain received), followed by a space, followed by the address. An example of a count-paired domain might be "9001 discuss.leetcode.com".

We are given a list cpdomains of count-paired domains. We would like a list of count-paired domains, (in the same format as the input, and in any order), that explicitly counts the number of visits to each subdomain.

Example 1:
Input:
["9001 discuss.leetcode.com"]
Output:
["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]
Explanation:
We only have one website domain: "discuss.leetcode.com". As discussed above, the subdomain "leetcode.com" and "com" will also be visited. So they will all be visited 9001 times.

Example 2:
Input:
["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
Output:
["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
Explanation:
We will visit "google.mail.com" 900 times, "yahoo.com" 50 times, "intel.mail.com" once and "wiki.org" 5 times. For the subdomains, we will visit "mail.com" 900 + 1 = 901 times, "com" 900 + 50 + 1 = 951 times, and "org" 5 times.

Notes:

The length of cpdomains will not exceed 100.
The length of each domain name will not exceed 100.
Each address will have either 1 or 2 "." characters.
The input count in any count-paired domain will not exceed 10000.
The answer output can be returned in any order.
"""

######################################################################################################
"""
The following code works to extract the list of count-paired domain for all subdomains listed in the input. 
The code works by maintaining a dictionary of subdomain and their visit count, such that every time a subdomain
is encountered, it's count is updated in the dictionary.
"""
######################################################################################################


def count_cp_domains(visitor_list):
    """
    Method to extract the count-paired domain for all subdomains listed in the input
    :param visitor_list: list of count-paired domains (as string) for main domains
    :return: list of count-paired domain for all subdomains listed in the input
    """
    count_dict = dict()  # Initialize and empty dictionary to store the visit count of each sub domain  -> O(1)

    for visitor in visitor_list:  # -> O(n)
        count, main_domain = visitor.split()  # Split the string at space to get the count and the main domain name -> O(1)
        count = int(count)  # Convert string  count to integer -> O(1)

        # If the domain is found in count dictionary, add the count to the same key, else create a new key with the
        # count as value  -> O(1)
        if main_domain in count_dict:
            count_dict[main_domain] += count
        else:
            count_dict[main_domain] = count

        all_sub_domains = main_domain.split(".")  # Split at . to extract elements of the domain -> O(m) Assuming domain has m characters

        # Iterate over the elements of the subdomain and update count for each subdomain -> O(x)  Assuming x elements in domain. Note that always x < m, so this will be covered in O(m) time at-most. Thus complexity is O(m) for this loop
        for i in range(1, len(all_sub_domains)):
            subdomain = ".".join(all_sub_domains[i:])  # Join all elements of the subdomain list from index i to last to create a subdomain

            # If the subdomain is found in count dictionary, add the count to the same key, else create a new key with
            # the count as value  -> O(1)
            if subdomain in count_dict:
                count_dict[subdomain] += count
            else:
                count_dict[subdomain] = count

    # Join the key value in count dictionary to create output list
    output = [str(c) + " " + domain for domain, c in count_dict.items()]  # -> O(n) or O(m) whichever is larger
    return output


"""
Total time complexity = Since we have a loop with O(n) complexity, and another loop inside it with O(m) complexity,
the upper bound will be O(n)*O(m) = O(n*m)
"""

# Test
print(count_cp_domains(["9001 discuss.leetcode.com"]))  #  ["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]
print(count_cp_domains(["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]))  #  ['900 google.mail.com', '901 mail.com', '951 com', '50 yahoo.com', '1 intel.mail.com', '5 wiki.org', '5 org']

