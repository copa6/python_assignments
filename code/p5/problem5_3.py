"""
3. Parse a file to count how many times email address is found, phone number is found, zip code is found
"""

######################################################################################################
"""
The following code extracts email, phone number and zipcode from a text file using regular expression matching for each
"""
######################################################################################################

import re


def read_file_data(file_path):
    """
    Helper method to read data from file into astring
    :param file_path: path to the input file
    :return: string data from file
    """
    with open(file_path, 'r') as f:
        data = f.read()
    return data


def count_regular_expression_in_text(data, match_pattern):
    """
    Helper method to count the number of regular expression matches in a string of data
    :param data: string containing the data
    :param match_pattern: regular expression pattern to match
    :return: count of regular expression matches in data
    """
    data = data.lower().replace("\n", " ")  # lower case the data and remove newline occurances
    matches = re.findall(match_pattern, data)  # Find all matches of the re pattern in data
    # print(matches)  # Uncomment this line to see matched examples
    return len(matches)


def count_email_address(file_path):
    """
    Method to count the number of email addresses in a file
    :param file_path: path to the file containing the input data
    :return: count of email addresses in the file
    """
    file_data = read_file_data(file_path)  # Read the file

    # Regular expression to detect email
    # []: allow all the fields within the []
    # a-z: Allow occurances of alphabets in a to z range
    # 0-9: Allow occurence of digits in range 0-9
    # ._%+-: Allow . _ % + and - sign
    # []+: Indicates one or more occurances
    # @: Followed by @
    # []+: Allow one or more occurence of alphabets, digits, . and -
    # \. : Followed by a .
    # [a-z]{2, }: Indicates 2 or more alphabets (Used to identify the domain)
    email_pattern = r'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}'
    email_count = count_regular_expression_in_text(file_data, email_pattern)
    return email_count


def count_phone_numbers(file_path):
    """
    Method to count the number of phone numbers in a file
    :param file_path: path to the file containing the input data
    :return: count of phone numbers in the file
    """
    file_data = read_file_data(file_path)   # Read the file
    # Regular expression to match phone number
    # First one matches local numbers - \([0-9]{3}\)\s?[0-9]{3}-[0-9]{4}
    # \( : Matches open (
    # [0-9]{3} : Matches 3 digits in 0-9 range
    # \) : Match close )
    # \s? : Match zero or one occurence of \s
    # [0-9]{3} : Matches 3 digits in 0-9 range
    # - : Followed by a - sign
    # [0-9]{4} : Matches 4 digits in 0-9 range

    # | is used as an OR sign to check either first or second expression match

    # Second one matches international numbers - \+[0-9]{1,2}-[0-9]{3}-[0-9]{3}-[0-9]{4}
    # \+ : Matches a + sign in the starting of the string
    # [0-9] : Matches 1 or 2 digits in 0-9 range
    # - : Matches a - sign
    # [0-9]{3} : Matches 3 digits in 0-9 range
    # - : Followed by a - sign
    # [0-9]{4} : Matches 4 digits in 0-9 range
    phone_num_pattern = r'\([0-9]{3}\)\s?[0-9]{3}-[0-9]{4}|\+[0-9]-[0-9]{3}-[0-9]{3}-[0-9]{4}'
    phone_count = count_regular_expression_in_text(file_data, phone_num_pattern)
    return phone_count


def count_zip_codes(file_path):
    """
    Method to count the number of zipcode in a file
    :param file_path: path to the file containing the input data
    :return: count of zipcodes in the file
    """
    file_data = read_file_data(file_path)   # Read the file
    zipcode_pattern = r'[0-9]{5}'  # Regular expression to match zip code - 5 numbers in range 0-9
    zipcode_count = count_regular_expression_in_text(file_data, zipcode_pattern)
    return zipcode_count


print(count_email_address("p3.txt"))  # Output: 4
print(count_phone_numbers("p3.txt"))  # Output: 4
print(count_zip_codes("p3.txt"))  # Output: 3




