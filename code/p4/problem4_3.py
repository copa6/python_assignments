"""
3. Largest subset whose all elements are Fibonacci numbers

Given an array with positive number the task is that we find largest subset from array that contain elements which are Fibonacci numbers.

Asked in Facebook

Examples :

Input : arr[] = {1, 4, 3, 9, 10, 13, 7};
Output : subset[] = {1, 3, 13}
The output three numbers are Fibonacci
numbers.

Input  : arr[] = {0, 2, 8, 5, 2, 1, 4,
                  13, 23};
Output : subset[] = {0, 2, 8, 5, 2, 1,
                    13, 23}
"""

######################################################################################################
"""
The code works by first creating the fibonacci sequence of numbers upto the maximum value of input array, and then 
checking if each element of the input array is present in fibonaccin sequence or not.
Elements that are present in the master fibonacci sequence are subset and returned.
"""
######################################################################################################


def get_fibonacci(max_n):
    """
    Helper function to get fibonacci numbers upto a max limit
    :param max_n: max limit
    :return: list containing fibonacci numbers
    """
    fib = [0, 1]  # Initialize fibonacci array
    num = 1  # Initialize first number

    # While the last number is less than max limit, add last two elements of the list and append.
    while fib[-1] <= max_n:
        num = fib[-1] + fib[-2]
        fib.append(num)
    return fib


def extract_fib_subset_from_arr(arr):
    """
    Method to extract fibonacci subset from input array.
    This works by first creating fibonacci sequence upto the max value of input array, and subsets elements by matching
    them with the fibonacci sequence
    :param arr: input array of numbers
    :return: subset of array with elements that are a part of the fibonacci sequence
    """
    max_value = max(arr)  # Get the maximum value from input array
    fib = get_fibonacci(max_value)  # Get fibonacci sequence up to max value
    fib_subset = []  # Initialize empty list to hold subset

    # for each element in input array, if it is a part of the fibonacci sequence, append to return data and return
    for i in arr:
        if i in fib:
            fib_subset.append(i)

    return fib_subset


# Tests
print(extract_fib_subset_from_arr([1, 4, 3, 9, 10, 13, 7]))   # Output -> [1, 3, 13]
print(extract_fib_subset_from_arr([0, 2, 8, 5, 2, 1, 4, 13, 23]))   # Output -> [0, 2, 8, 5, 2, 1, 13]
# ## NOTE: For test 2, example is incorrect as 23 is not a fibonacci number

