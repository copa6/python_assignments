"""
Converting Roman Numerals lying between 1 to 3999 to Decimal Numbers
"""

######################################################################################################
"""
The code converts a roman numeral to it's integer representation using the following steps - 
1. Initialize number to 0.
2. Iterate over a map of roman numerals counting the occurence of each packet from the left and adding it to the number.
"""
######################################################################################################

# integers = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
# romans = ["I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"]

integers = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
romans = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]


def convert_roman_to_int(roman_numeral):
    """
    Function to convert roman numeral to it's equivalent integer representation
    :param roman_numeral: Roman numeral to be converted
    :return: integer representation of the roman numeral
    """
    num = 0  # Initialize number to 0

    # Iterate over map of all roman numbers in list
    for i, roman_base in enumerate(romans):
        base_size = len(roman_base)  # Get the size of the roman numeral (1 or 2)

        # If the roman number starts with the base, add equivalent amount to the output integer number
        while roman_numeral.startswith(roman_base):
            num += integers[i]
            roman_numeral = roman_numeral[base_size:]  # Remove first 1/2 digits(size of base number) from the roman numeral and continue to next iteration

    return num


# Tests
print(convert_roman_to_int("IX"))  # Output: 9
print(convert_roman_to_int("XL"))  # Output: 40
print(convert_roman_to_int("MCMIV"))  # Output: 1904
print(convert_roman_to_int("MMCMXCIX"))  # Output: 2999
print(convert_roman_to_int("MMMXIX"))  # Output: 3019
