# coding=utf-8
"""4. Calculate maximum value using ‘+’ or ‘*’ sign between two numbers in a string Last Updated: 23-07-2019 Given a
string of numbers, the task is to find the maximum value from the string, you can add a ‘+’ or ‘*’ sign between any
two numbers.

Examples:

Input : 01231
Output :
((((0 + 1) + 2) * 3) + 1) = 10
In above manner, we get the maximum value i.e. 10

Input : 891
Output :73
As 8*9*1 = 72 and 8*9+1 = 73.So, 73 is maximum.
"""

######################################################################################################
"""
The code works using the logic  that whenever we encounter a 0 or a 1, we need to use + to add, as these will not aid in
anyway  if we multiply. For all other digits, we use the multiplication operation. 
 
It has the following steps - 
1. Split all characters in string and store in a list.
2. Maintain a running sum by assigning the first digit to it.
2. Starting with the second digit - 
    - If the running sum is a 0 or 1, we use a + sign to add it to the running sum.
    - If next digit is 0 or 1, we use a + sign to add it to the running sum.
    - For digits greater than 1, use * to multiply.
"""
######################################################################################################


def get_maximum(s):
    """
    Function to get the max value using + and * sign between digits of a string.
    :param s: string containing digits
    :return: maximum value obtained using + and *
    """
    digits = list(s)  # Extract the digits in the string into a list
    max_val = int(digits[0])  # Convert to integer and place the first digit into output variable

    # Starting from second digit add or multiply it to max value depending on the digit and max_val's current value
    for d in digits[1:]:
        d = int(d)  # Convert string digit to integer
        # If current digit, or max_val is 1 or 0, add it to the max_val, else multiply
        if d <= 1 or max_val <= 1:
            max_val += d
        else:
            max_val *= d

    return max_val


# Tests
print(get_maximum("01231"))  # Output -> 10
print(get_maximum("891"))  # Output -> 73

