"""
6. Find subarray with given sum | Set 1 (Nonnegative Numbers)
Last Updated: 25-05-2020
Given an unsorted array of nonnegative integers, find a continuous subarray which adds to a given number.
Examples :

Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Ouptut: Sum found between indexes 2 and 4
Sum of elements between indices
2 and 4 is 20 + 3 + 10 = 33

Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Ouptut: Sum found between indexes 1 and 4
Sum of elements between indices
1 and 4 is 4 + 0 + 0 + 3 = 7

Input: arr[] = {1, 4}, sum = 0
Output: No subarray found
There is no subarray with 0 sum
There may be more than one subarrays with sum as the given sum. The following solutions print first such subarray.
"""

######################################################################################################
"""
The following code finds the index of subarray that adds to a desired sum. 
The code works with the followig logic - 
1. Iterate over the elements of the array and maintain a running sum.
2. If the running sum becomes more than the desired sum, subtract elements from the beginning of the array until the 
running sum is less than or equal to the desired sum
3. At any point, if the running sum and desired sum are equsal, return the starting and ending index of the running sum. 
"""
######################################################################################################


def find_subarray_with_sum(arr, desired_sum):
    """
    Function to find subarray with desired sum.
    :param arr: array to loop over and find subarray adding to desired sum
    :param desired_sum: desired sum
    :return: string with start and end index of the subarray that adds to desired sum. If no such subarray exists,
            return "No subarray found"
    """
    running_sum = 0  # Initialize a running sum to 0
    start = 0  # Initialize the starting index to 0

    for i, num in enumerate(arr):  # Loop over each element of the array
        running_sum += num  # Add current element to the running sum

        # If the running sum is greater than the desired sum, subtract one element at a time, from the beginning of
        # the array, and updated the start index
        while running_sum > desired_sum:
            running_sum -= arr[start]
            start += 1

        # If at any point the desired sum and running sum are equal, return the start and current index indicating
        # subarray with desired sum
        if running_sum == desired_sum:
            return "Sum found between index {0} and {1}".format(start, i)

    # If no subarray with desired sum exists, return appropriate message
    return "No subarray found"


# Test
print(find_subarray_with_sum([1, 4, 20, 3, 10, 5], 33))  # Sum found between index 2 and 4
print(find_subarray_with_sum([1, 4, 0, 0, 3, 10, 5], 7))  # Sum found between index 1 and 4
print(find_subarray_with_sum([1, 4], 7))  # No subarray found
