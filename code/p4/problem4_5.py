"""
5. Decode a string recursively encoded as count followed by substring
Last Updated: 23-04-2020
An encoded string (s) is given, the task is to decode it. The pattern in which the strings are encoded is as follows.

<count>[sub_str] ==> The substring 'sub_str'
                      appears count times.
Examples:

Input : str[] = "1[b]"
Output : b

Input : str[] = "2[ab]"
Output : abab

Input : str[] = "2[a2[b]]"
Output : abbabb

Input : str[] = "3[b2[ca]]"
Output : bcacabcacabcaca

"""

######################################################################################################
"""
The following code works using regular expressions to decode the strin recursively, from the innermost substring, moving
up the branch at each recursion. It has the following flow - 
1. Extract elements that match the pattern - <digit>[<characters>] from the input string. This extracts only the 
innermost elements.
2. Expand the character set by multiplying it n times and replace the pattern with expanded character set in the string.
3. Repeat steps 1 & 2 until no matching pattern exists in the string.
"""
######################################################################################################

# Use regular expressions for pattern matching
import re


def expand_string(s):
    """
    Method to expand encoded string s using recursion
    :param s: encoded string
    :return: decoded string
    """
    # Encoding pattern -
    # \d - Indicates a digit
    # \[ - Indicates opening bracket
    # \w - Indicates a character
    # + - Indicates one or more occurences
    # \] - Indicates a closing bracket
    expandable_section = r'\d\[\w+\]'

    matched_section = re.findall(expandable_section, s)  # Finds all matching patterns in the string
    if len(matched_section) > 0:

        # For each matched patterns sub-string, extract it's count and repeat n times to decode
        for elem in matched_section:
            count = int(elem[0])  # Extract count of substring
            sub_str = elem[2:-1]*count  # Multiply the sub-string characters count times to decode
            s = s.replace(elem, sub_str)  # replace the encoded part of the string with it's decoded equivalent
        expanded_string = expand_string(s)  # Recursively call the function to decode the next level
    else:
        # If there are no matching patterns, the string is fully decoded and is returned as is
        expanded_string = s

    return expanded_string


# Tests
print(expand_string("1[b]"))  # Output: b
print(expand_string("2[ab]"))  # Output: abab
print(expand_string("2[a2[b]]"))  # Output: abbabb
print(expand_string("3[b2[ca]]"))  # Output: bcacabcacabcaca
print(expand_string("3[b2[ca]3[bb]]"))  # Output: bcacabbbbbbbcacabbbbbbbcacabbbbbb


