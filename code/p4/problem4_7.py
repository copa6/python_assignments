######################################################################################################
"""
The following code works to identify the time to 1billion users, given growth rates of multiple apps.

The first algorithm works by linearly checking from 1 to n, when the total number of users reach 1 billion. This is not
the best algorithm as it does not scale well when the growth rate is close to 1.000001.

The optimized algorithm works in the following way -
1. Start from 1 and increment by *2 with every iteration until the number of users is more than required number of users (1 billion in our case)
2. When the number of users increases the desired number of users, we know that the t at which users cross 1 billion
lies between current value to t and t/2.
3. We check whether growth at t/4 lies above or below the 1 billion point, and select the right t between t/2 to t/4,
ot t/4 to t.
4. Repeating step 3 until we reach a point where we find t, t-1 such that at t-1 the total number of users is less than
1b, and at t it crosses 1b
"""
######################################################################################################
import math  # Use math library to get infinity as placeholder


def find_time_to_n_users_naive(growth_rates, n=1000000000):
    """
    Method to naively check time to reach n, given growth rates of multiple applications.
    NOTE - THIS METHOD IS VERY SLOW AS GROWTH RATE REDUCES
    :param growth_rates: list of growth rates of various apps
    :param n: limit to check. default 1 billion
    :return: time to reach n, given the application growth rates
    """
    users = sum(growth_rates)  # Get base growth rates at t=1
    t = 1

    # Iteratively check when number of users crosses n
    while users < n:
        t += 1
        users = sum([g**t for g in growth_rates])  # Calculate sum of users at time t
    return t


def find_time_to_n_users_optimized(growth_rates, n=1000000000):
    """
    Optimized algorithm to check the time to reach n users, given growth rates of applications
    :param growth_rates: list of growth rates of various apps
    :param n: limit to check. default 1 billion
    :return: time to reach n, given the application growth rates
    """
    t = 1  # Initialize starting time t=1
    t_min = 1  # Initialize minimum time to 1
    t_max = math.inf  # Initialize maximum time to infinity

    # Use a tree like structure to check time to n users by checking at midpoints between t-max and t-min
    while 1:
        users = sum([g**t for g in growth_rates])    # Calculate sum of users at time t

        # If number of users at time t is less than n, assign t_min to t, and double t
        if users < n:
            t_min = t
            t = min(t*2, t_max)

        # If number of users is more than n, assign t_max to current t, and assign t to value between t_min and t_max
        else:
            t_max = t

            # If difference between t_min and t_max is 1, it indicates a transition point from less than n users to
            # more than n users. This is our desired answer
            if t_max-t_min == 1:
                return t_max
            else:
                t = int((t_max + t_min)/2)  # Else assign t to value between t_min and t_max


# Tests
print(find_time_to_n_users_optimized([1.0000000001, 1.0000000002]))  # Output: 103616162508
print(find_time_to_n_users_optimized([1.01, 1.02]))  # Output: 1047
print(find_time_to_n_users_optimized([1.5]))  # Output: 52
print(find_time_to_n_users_optimized([1.1, 1.2, 1.3]))  # Output: 79

