"""
Converting Decimal Number lying between 1 to 3999 to Roman Numerals
"""

######################################################################################################
"""
The code works by isolating individiual elements of the roman numeral, from the integer representation and converts them
one at a time.
It has the following flow - 
1. Initialize two lists to hold the main integer and their corresponding roman representations
2. Starting with the largest number in integer representation, a floored division gives the count of the largest integer
in the number.
3. Add equivalent number of roman representation at the same index.
4. Subtract the largest integer from the number and repeat.


 Example: 3581 has 3 packtes of 1000, one of 500, one of 50, three of 10 and a 1
 It's roman representation is  - 3*M, 1*D, 1*L, 1*X, 1*I -> MMMDLXI
"""
######################################################################################################

integers = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
romans = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]


def convert_int_to_roman(num):
    """
    Method to convert integer number to roman representation
    :param num: integer number between 1-3999
    :return: roman numeral representation of the integer
    """
    roman_reps = []  # Initialize list to hold roman representation

    # Get count of each of integer "packets" in the number.
    for i, base in enumerate(integers):
        base_count = num//base  # Get count of integer packet in the number.
        roman_reps.append(romans[i]*base_count)  # Add roman representation of the packet to output
        num -= base_count*base  # Divide the number by the integer packet for next iteration

    return "".join(roman_reps)


# Tests
print(convert_int_to_roman(9))  # Output: IX
print(convert_int_to_roman(40))  # Output: XL
print(convert_int_to_roman(1904))  # Output: MCMIV
print(convert_int_to_roman(2999))  # Output: MMCMXCIX
print(convert_int_to_roman(3019))  # Output: MMMXIX
