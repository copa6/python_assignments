"""
Find all triplets with zero sum

Given an array of distinct elements. The task is to find triplets in the array whose sum is zero.

Examples :

Input : arr[] = {0, -1, 2, -3, 1}
Output : (0 -1 1), (2 -3 1)

Explanation : The triplets with zero sum are
0 + -1 + 1 = 0 and 2 + -3 + 1 = 0

Input : arr[] = {1, -2, 1, 0, 5}
Output : 1 -2  1
Explanation : The triplets with zero sum is
1 + -2 + 1 = 0
"""

######################################################################################################
"""
The following code works by extracting all n element combinations from the list and checking which of these combinations 
add up to the desired sum.
"""
######################################################################################################


def find_elements_with_desired_sum(arr, subset_len=3, desired_sum=0):
    """
    Method to find sebarrays with the desired sum
    :param arr: input array of integers
    :param subset_len: size of subarray to check. default: 3
    :param desired_sum: desired sum. default: 0
    :return: list of subarrays that add up to the desired_sum
    """
    from itertools import combinations  # Use itertools to generate combinations of the input array elements
    output_subarrays = []  # Initialize list to hold output subarrays

    # For all n-element combinations of the input list, check if the sub-array adds to desired sum and append to output list
    for subset_combination in combinations(arr, subset_len):
        if sum(subset_combination) == desired_sum:
            output_subarrays.append(subset_combination)

    return output_subarrays


# Tests
print(find_elements_with_desired_sum([0, -1, 2, -3, 1]))  # Output: [(0, -1, 1), (2, -3, 1)]
print(find_elements_with_desired_sum([1, -2, 1, 0, 5]))  # Output: [(1, -2, 1)]
