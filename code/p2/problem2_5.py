# coding=utf-8
"""
5. Implement atoi which converts a string to an integer.

The function first discards as many whitespace characters as necessary until the first non-whitespace character is found. Then, starting from this character, takes an optional initial plus or minus sign followed by as many numerical digits as possible, and interprets them as a numerical value.

The string can contain additional characters after those that form the integral number, which are ignored and have no effect on the behavior of this function.

If the first sequence of non-whitespace characters in str is not a valid integral number, or if no such sequence exists because either str is empty or it contains only whitespace characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned.

Note:

Only the space character ' ' is considered as whitespace character.
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. If the numerical value is out of the range of representable values, INT_MAX (231 − 1) or INT_MIN (−231) is returned.
Example 1:

Input: "42"
Output: 42
Example 2:

Input: "   -42"
Output: -42
Explanation: The first non-whitespace character is '-', which is the minus sign.
             Then take as many numerical digits as possible, which gets 42.
Example 3:

Input: "4193 with words"
Output: 4193
Explanation: Conversion stops at digit '3' as the next character is not a numerical digit.
Example 4:

Input: "words and 987"
Output: 0
Explanation: The first non-whitespace character is 'w', which is not a numerical
             digit or a +/- sign. Therefore no valid conversion could be performed.
Example 5:

Input: "-91283472332"
Output: -2147483648
Explanation: The number "-91283472332" is out of the range of a 32-bit signed integer.
             Thefore INT_MIN (−231) is returned.
"""

######################################################################################################
"""
The solution works to extract integer from the beginning of input string in the following steps - 
1. Strip all leading and trailing whitestpaces using strip()
2. get a list of words in the string by splitting at space
3. Use the inbuilt re module (Regular Expression) to extract +/- sign followed by a set of digits, from the first element in the list 
"""
######################################################################################################
import re


def extract_integer_from_string_start(s):
    """
    Function to extract integer from starting of a string
    :param s: string to extract the integer from
    :return: Extracted integer if it exists (within machine bit range), else 0
    """
    words = s.strip().split(" ")  # sremove leading and trailing whitespaces and split the string at internal spaces

    # Prepare regular expression to extract integer from string
    # ^ - Indicates that the code should start looking at the start of the string
    # [+-] - Checks for either + or - at the beginning
    # ? - Indicates to look for zero or one occurence of +/-
    # \d - Indicated digits zero to nine
    # + - Indicates to check for one or more continuous occurences of digits
    integer_match_regexp = r"^[+-]?\d+"

    integer_matches = re.findall(integer_match_regexp, words[0]) # Use re module to find all  instance that match the regular expression
    if len(integer_matches)>0:
        num = int(integer_matches[0])  # If a match is found, extract the first element from match (index 0)

        # Clipping the output at 2^31 range
        if num > 0:
            return min(num, 2**31)
        else:
            return max(num, -2**31)
    else:
        return 0


# Test cases
print(extract_integer_from_string_start("42"))  # Output -> 42
print(extract_integer_from_string_start("   -42"))  # Output -> -42
print(extract_integer_from_string_start("4193 with words"))  # Output -> 4193
print(extract_integer_from_string_start("words and 987"))  # Output -> 0
print(extract_integer_from_string_start("-91283472332"))  # Output -> -2147483648
