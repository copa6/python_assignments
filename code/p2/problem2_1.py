"""
1. Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.

The order of output does not matter.

Example 1:

Input:
s: "cbaebabacd" p: "abc"

Output:
[0, 6]

Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".
Example 2:

Input:
s: "abab" p: "ab"

Output:
[0, 1, 2]

Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
"""

######################################################################################################
"""
The solution works in the following steps - 
1. Using inbuilt itertool library, get all permutations(anagrams) of the substring p
2. For each anagram, if it exists in the string - 
    - Get the index of anagram and store in a list.
    - Check for further occurences of the anagram in the string, starting from index of previous occurrence of the 
    anagram, and update the indices list
3. Repeat step 2 for each anagram and create a master list of indices of all anagrams.
4. Convert the list from step 2, to a set to filter out the unique indices, and return
"""
######################################################################################################
import itertools


def get_substring_indices(search_string, substring):
    """
    Helper function to find indices of substring occurrence in main string
    :param search_string: Main string in which substring needs to be searched
    :param substring: substring to be searched
    :return: list of indices of substring in search_string
    """
    substr_indices = []  # Initialize empty list to store list of substring indices
    prev_idx = 0  # Initialize variable to decide how much of the string to be searched
    string_len = len(search_string)

    # continue extracting substring indices while they exist in the search string
    while substring in search_string:
        idx = search_string.index(substring)  # Find the index of substring in search string
        substr_indices.append(prev_idx + idx)  # Add prev_idx while appending, as we slice the string at prev_idx for next iteration
        prev_idx += idx + 1  # slicing index for next iteration
        prev_idx = min(string_len, prev_idx)  # Clipping at length of string
        search_string = search_string[prev_idx:]  # Slice search string for next iteration

    return substr_indices


def get_anagram_indices(s, p):
    """
    Function to get indices of anagrams of string p in string s
    :param s: Main string in which substring is to be searched
    :param p: substring used for search
    :return: list of indices of anagrams of p in s
    """
    anagrams_p = ["".join(elems) for elems in itertools.permutations(p)]  # Get all anagrams of p

    match_indices = []  # Initialize list to hold indices of anagrams

    # For each anagram of p, use the helper function defined above to get list of all indices of anagrams of p in s
    for anagram in anagrams_p:
        if anagram in s: # Only call helper if anagram exists in s, saving on computation
            anagram_match_indices = get_substring_indices(s, anagram)
            match_indices += anagram_match_indices

    unique_match_indices = list(set(match_indices))  # Use set to filter out unique indices
    return unique_match_indices


# Test cases
print(get_anagram_indices("cbaebabacd", "abc"))  # Output -> [0,6]
print(get_anagram_indices("abab", "ab"))  # Output -> [0, 1, 2]
