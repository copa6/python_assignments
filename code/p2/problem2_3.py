"""
3. Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true
Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
"""

######################################################################################################
"""
The solution checks if an integer is a palindrome by 
1. converting the integer to string, 
2. Reversing the string
3. Convert back from string to integer

If subtracting the result of step 3 with the original integer gives 0, then the number is a palindrome. Else it is not 
"""
######################################################################################################


def check_if_integer_palindrome(num):
    """
    Function to check if a number is a palindrome.
    :param num: Number to be checked
    :return: boolean indicating whether the input number is a palindrome or not
    """
    # If the number is negative, it can not be a palindrome due to the leading '-' sign
    if num < 0:
        return False

    num_reverse = int(str(num)[::-1])  # Convert to string, reverse using [::-1] and convert back to int
    if num-num_reverse == 0:  # If difference of original number and the reversed number is 0, then return True, else False
        return True
    return False


print(check_if_integer_palindrome(10))  # Output -> False
print(check_if_integer_palindrome(121))  # Output -> True
print(check_if_integer_palindrome(-121))  # Output -> False
print(check_if_integer_palindrome(1923291))  # Output -> True
