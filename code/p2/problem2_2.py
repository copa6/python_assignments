"""
2. Given a string, determine if a permutation of the string could form a palindrome.

Example 1:

Input: "code"
Output: false
Example 2:

Input: "aab"
Output: true
Example 3:

Input: "carerac"
Output: true
"""

######################################################################################################
"""
Solution works on the logic that for a string to be a palindrome, there should be even number of occurrence of all 
characters in the string, with at most one character which occurs odd number of times. It has the following flow - 
1. Get character count of all characters in the string.
2. If there are more than one odd number in the character count list, then the string is not a palindrome, else it is  
"""
######################################################################################################


def check_if_string_palindrome(s):
    """
    Function to check if a string, aor any of it's anagrams is a palindrome
    :param s: string to check for palindrome
    :return: boolean indicating the palindrome status of s
    """
    from collections import Counter
    character_counts_in_string = Counter(s).values()  # Use counter to get character counts in string s. Extract just the count values
    odd_counts = 0  # Keep a count of number of odd occurrences

    # Check all counts and add to odd_counts variable. If more than one odd counts exist, return false.
    for count in character_counts_in_string:
        if count%2 == 1:
            odd_counts += 1
        if odd_counts > 1:
            return False

    # If there are fewer than 1 odd characters in string, return True
    return True


# Test cases
print(check_if_string_palindrome("code"))  # Output -> False
print(check_if_string_palindrome("aab"))  # Output -> True
print(check_if_string_palindrome("carerac"))  # Output -> True

