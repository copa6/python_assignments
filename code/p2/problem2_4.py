"""
4. Say you have an array prices for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Example 1:

Input: [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
             Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
Example 2:

Input: [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
             Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
             engaging multiple transactions at the same time. You must sell before buying again.
Example 3:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.


Constraints:

1 <= prices.length <= 3 * 10 ^ 4
0 <= prices[i] <= 10 ^ 4

"""

######################################################################################################
"""
For the solution, the code works by find clusters of increasing prices and taking the difference between them to 
calculate profits. 
1. Consider the index 0 to be lowest price day.
2. Iterate over all elements from index 1 to last - 
    - if the current element value is greater than value at lowest price day, update lowest price day to current index.
    - Check if the current element value is the local peak, that is, greater than both it's neighbours (or just 
    left neighbour for last element).
        For peak element, calculate the difference between lowest price day, and current element and add to profits  

"""
######################################################################################################


def get_max_profits(prices):
    """
    Function to calculate max profit that can be obrtained from a list of stock prices
    :param prices: list of stock prices for consecutive days
    :return: max profit that can be obtained by trading stock price list
    """
    profit = 0  # Initialize profit to 0
    num_days = len(prices)  # Get a count of the number of elements in price list

    min_price_day = 0  # Consider index 0 to be a local minima price

    # Check for local peaks from index 1 to end of list
    for i in range(1, num_days):

        # If price at current index is lesser than price at last index, update min_price_day variable to point to current index
        if prices[i] < prices[i-1]:
            min_price_day = i

        # If price forms a peak (greater than both it's neighbours), sell on that day and add to profits
        if prices[i] >= prices[i-1] and (i==num_days-1 or prices[i]>prices[i+1]):
            profit += prices[i] - prices[min_price_day]

    return profit


# Test cases
print(get_max_profits([7,1,5,3,6,4]))  # OUTPUT -> 7
print(get_max_profits([1,2,3,4,5]))  # OUTPUT -> 4
print(get_max_profits([7,6,4,3,1]))  # OUTPUT -> 0

