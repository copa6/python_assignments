"""
2. reverse linked list

Given a linked list of size N. The task is to reverse every k nodes (where k is an input to the function) in the linked list.

Example 1:

Input:
LinkedList: 1->2->2->4->5->6->7->8
K = 4
Output: 4 2 2 1 8 7 6 5
Example 2:

Input:
LinkedList: 1->2->3->4->5
K = 3
Output: 3 2 1 5 4
"""
######################################################################################################
"""
The following function reverses a linked list every k nodes. 
The function works in the following steps - 
1. Starting with the head of the LL, keep moving each next node to head for k iterations.
2. Every k iterations, shift forward by k nodes, and insert every next node at k-th position.
"""
######################################################################################################

from copy import deepcopy


class Node:
    """
    Class to define node of a linked list
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    """
    Class to implement a linked list in python
    """
    def __init__(self, data=None):
        """
        Initialize the linked list with None at it's head
        """
        self.head = None
        if data:
            self.create(data)

    def create(self, list_data):
        """
        Creates a linked list from input list of elements
        :param list_data: data to be put into linked list
        :return: None
        """
        for data in list_data[::-1]:
            self.push(data)

    def push(self, data):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param data: single element to be added to the linked list
        :return: None
        """
        node = Node(data)  # Create a new node instance
        node.next = self.head  # Mark the node to be current head's previous node
        self.head = node  # Update reference of head to current node

    def reverse(self):
        previous_node = None  # Mark previous instance as None. This will be used as the tail of reversed LL
        current_node = self.head  # Start with the head of the LL, and reverse .next mapping one node at a time

        # Iterate till the tail (None) of the LL
        while current_node:
            next_node = current_node.next  # Get a reference to the next element in the LL
            current_node.next = previous_node  # Reverse current node's next attribute to point to the previous node

            # Move one step to the right for next iteration
            previous_node = current_node
            current_node = next_node

        self.head = previous_node  # Mark the last node as the head of the reversed linked list

    def show(self):
        """
        Helper method to print the linked list instance
        :return:
        """
        node = self.head
        print_output = []
        while node:
            print_output.append(str(node.data))
            node = node.next
        print("->".join(print_output))


def partially_reverse_linked_list(ll, k):
    """
    Function to reverse a linked list at every k nodes
    :param ll: input linked list
    :param k: nodes which need to be reversed
    :return: input linked list reversed at every k nodes
    """
    ll_copy = deepcopy(ll)  # Deep copy the input list to not override the input
    head = ll_copy.head  # Get the head of the LL - This forms the reference for each node insertion for reversing
    previous_node = head  # Mark head as the previous node
    current_node = head.next  # Mark the next node as current node
    starter = None  # Placeholder for started node - Used to move the insertion node forward by k nodes
    iter = 0  # Iteration counter

    # Iterate till the end of LL
    while current_node:
        node_iterator = k  # Copy k to a local counter

        # Iterate for k iterations and reverse
        while node_iterator > 1:

            # If the end of the LL is reached, break out of loop
            if current_node is None:
                break

            next_node = current_node.next  # Get reference to next node
            current_node.next = head  # Move current node to the head of the LL, and mark head as it's net node
            previous_node.next = next_node  # Fill the gap created by moving current node, by connecting previous node with next node

            # Initialize nodes for next iteration of the loop
            head = current_node  # Move head left to current node
            current_node = next_node  # Move current node forward to the next node
            node_iterator -= 1  # Decrement counter

            # From second iteration onwards, since point of reversal is in the middle of the LL, we need to remap the head
            if iter != 0:
                prev_head = starter.next  # Get previous head
                starter.next = head  # Connect current head to the node from last reversal round
                head.next = prev_head  # Connect current head and previous head

        # For first iteration, mark the last swapped point as head of LL
        if iter == 0:
            ll_copy.head = head

        # Map nodes for next iteration
        starter = previous_node  # Mark starter as the last node of previous reversal cycle
        previous_node = current_node  # Move previous right

        # If current node is not None, move it to next node for next iteration
        if current_node:
            current_node = current_node.next

        iter += 1  # Increment counter

    # Show locally reversed LL
    ll_copy.show()


# Test
ll = LinkedList([1, 2, 2, 4, 5, 6, 7, 8])
partially_reverse_linked_list(ll, 4)  # Output: 4->2->2->1->8->7->6->5

ll = LinkedList([1, 2, 3, 4, 5])
partially_reverse_linked_list(ll, 3)  # Output: 3->2->1->5->4


