# coding=utf-8
"""
6. given sum of 1d array

Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).

Return the running sum of nums.



Example 1:

Input: nums = [1,2,3,4]
Output: [1,3,6,10]
Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
Example 2:

Input: nums = [1,1,1,1,1]
Output: [1,2,3,4,5]
Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
Example 3:

Input: nums = [3,1,2,10,1]
Output: [3,4,6,16,17]
"""

######################################################################################################
"""
The following function gets the running sum of input array in the following steps - 
1. Initialize an empty array and a running_sum variable.
2. Iterate over all elements of the input list, adding the element value to the running_sum variable, and inserting it 
into the output list.
"""
######################################################################################################


def make_running_sum_array(arr):
    """
    Function to get running sum array from the input array
    :param arr: input array
    :return: list containing the running sum from input array
    """
    running_sum_arr = []  # Initialize empty array to store the running sum
    running_sum = 0  # Initialize running sum to 0
    for elem in arr:
        running_sum += elem  # Add current element value to running sum
        running_sum_arr.append(running_sum)  # Insert running sum to the output array

    return running_sum_arr


# Test
print(make_running_sum_array([1,1,1,1,1]))  # Output: [1,2,3,4,5]
print(make_running_sum_array([3,1,2,10,1]))  # Output: [3,4,6,16,17]
print(make_running_sum_array([1,2,3,4]))  # Output: [1,3,6,10]





