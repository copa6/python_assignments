"""
7. Given the array candies and the integer extraCandies, where candies[i] represents the number of candies that the
ith kid has.
For each kid check if there is a way to distribute extraCandies among the kids such that he or she can have the
greatest number of candies among them. Notice that multiple kids can have the greatest number of candies.
"""

######################################################################################################
"""
The following function identifies which kids could have the most candies if they are given extra candies. The function 
works in the following steps - 
1. Get the maximum value from the array. This is the maximum candies possible without extra candies.
2. For each entry in the list, add extra candies to the element and if it is greater than or equal to the maximum value,
mark that index as true.
"""
######################################################################################################


def identify_kids_with_most_candies(candies, extraCandies):
    """
    Function to identify kids with max candies if given extraCandies
    :param candies: List containing count of candies each kid has
    :param extraCandies: Number of extra candies available
    :return: Boolean array indicating which kids can have maximum candies if given extra candies
    """
    max_candies = max(candies)  # Identify the max candies with a kid before getting extra candies
    does_kid_get_most_candies = []  # Initialise empty list to store output booleans

    # For each entry in the list, add extra candies to the element and if it is greater than or equal to the maximum
    # value, mark that index as true.
    for candy in candies:
        does_kid_get_most_candies.append(candy+extraCandies >= max_candies)

    return does_kid_get_most_candies


# Test
print(identify_kids_with_most_candies([1, 2, 3, 4, 5], 3))  # Output: [False, True, True, True, True]
