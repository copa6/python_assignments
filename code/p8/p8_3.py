"""
3. Add two numbers represented by linked lists

Given two numbers represented by two linked lists of size N and M. The task is to return a sum list. The sum list is a linked list representation of the addition of two input numbers.

Example 1:

Input:
N = 2
valueN[] = {4,5}
M = 3
valueM[] = {3,4,5}
Output: 3 9 0
Explanation: For the given two linked
list (4 5) and (3 4 5), after adding
the two linked list resultant linked
list will be (3 9 0).
Example 2:

Input:
N = 2
valueN[] = {6,3}
M = 1
valueM[] = {7}
Output: 7 0
Explanation: For the given two linked
list (6 3) and (7), after adding the
two linked list resultant linked list
will be (7 0).
"""

######################################################################################################
"""
Function to add two integer numbers represented by linked lists. 
The function works in the following steps - 
1. Reverse the input linked lists such that the order of nodes is ones, tens, thousands ... - This makes it easy to 
carry over 1.
2. Starting with the head of both LL, add their values and push to a new output LL. If the sum is more than 10, mark 
carry variable and push value-10 into output LL.
3. Move one node at a time and add their values, checking for carry and pushing to output LL.
4. Once either LL's end is reached, add nodes from other LL to the output as is, adding carry if necessary.
"""
######################################################################################################


class Node:
    """
    Class to define node of a linked list
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    """
    Class to implement a linked list in python
    """
    def __init__(self, data=None):
        """
        Initialize the linked list with None at it's head
        """
        self.head = None
        if data:
            self.create(data)

    def create(self, list_data):
        """
        Creates a linked list from input list of elements
        :param list_data: data to be put into linked list
        :return: None
        """
        for data in list_data[::-1]:
            self.push(data)

    def push(self, data):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param data: single element to be added to the linked list
        :return: None
        """
        node = Node(data)  # Create a new node instance
        node.next = self.head  # Mark the node to be current head's previous node
        self.head = node  # Update reference of head to current node

    def reverse(self):
        previous_node = None  # Mark previous instance as None. This will be used as the tail of reversed LL
        current_node = self.head  # Start with the head of the LL, and reverse .next mapping one node at a time

        # Iterate till the tail (None) of the LL
        while current_node:
            next_node = current_node.next  # Get a reference to the next element in the LL
            current_node.next = previous_node  # Reverse current node's next attribute to point to the previous node

            # Move one step to the right for next iteration
            previous_node = current_node
            current_node = next_node

        self.head = previous_node  # Mark the last node as the head of the reversed linked list

    def show(self):
        """
        Helper method to print the linked list instance
        :return:
        """
        node = self.head
        print_output = []
        while node:
            print_output.append(str(node.data))
            node = node.next
        print("->".join(print_output))


def update_nodes(head, carry):
    """
    Helper function to update remaining nodes of an LL with carry
    :param head: head of the Ll whose elements need to be updated
    :param carry: carry variable. Can be either 1 or 0
    :return: None
    """
    # Update if head has data
    if head is not None:
        data = head.data

        # Add carry to data and update
        data += carry
        carry = 0

        # As long data is more than 10, subtract 10, and mark carry as 1. This ensures proper transitions when remaining
        # nodes are 9, 9, 9 ... and carry is 1
        while data >= 10:
            data -= 10
            head.data = data
            carry = 1

            # head has a next node, add 1 to it's data value, and continue
            if head.next:
                head = head.next
                data = head.data + 1
                carry = 0
        head.data = data

    # If carry is 1 after traversing all nodes of LL, insert a new node to hold the carry variable
    if carry:
        new_node = Node(1)
        head.next = new_node


def add_as_linked_lists(head_1, head_2):
    """
    Helper method to add two lls
    :param head_1: head reference of first ll
    :param head_2: head reference of second ll
    :return: head reference of the output ll
    """
    sum_list = LinkedList()  # Initialize empty LL to store the sum

    # If either LL is empty, mark the other LL as the output
    if head_1 is None:
        sum_list.head = head_2
    elif head_2 is None:
        sum_list.head = head_1

    # If both LL contain nodes
    else:
        node_sum = head_1.data + head_2.data  # Add the data in the head of both nodes
        carry = 0  # Initialize carry to 0

        # If sum is more than 10, mark carry and subtract 10 from the sum
        if node_sum >= 10:
            node_sum -= 10
            carry = 1

        sum_list.push(node_sum)  # Push the sum value to output. This inserts ones value in LL

        head_1 = head_1.next  # Move LL1 head to next node
        head_2 = head_2.next  # Move LL2 head to next node
        while True:
            # If both the LL's are exhausted, break from loop
            if head_1 is None and head_2 is None:
                break

            # If LL1 is exhausteds, append remaining elements of LL2 to the output
            if head_1 is None:
                update_nodes(head_2, carry)  # Update remaining nodes of LL2 to account for remaining carry element
                tail = sum_list.head  # Get reference to sum_list_head
                sum_list.reverse()  # Reverse the sum_list to insert  nodes from LL2 at the head
                tail.next = head_2  # Insert nodes from LL2 at head
                sum_list.reverse()  # Reverse output sum list again to put the nodes in right order
                break

            # If LL2 is exhausteds, append remaining elements of LL2 to the output
            if head_2 is None:
                update_nodes(head_1, carry)  # Update remaining nodes of LL1 to account for remaining carry element
                tail = sum_list.head  # Get reference to sum_list_head
                sum_list.reverse()  # Reverse the sum_list to insert  nodes from LL1 at the head
                tail.next = head_1  # Insert nodes from LL1 at head
                sum_list.reverse()  # Reverse output sum list again to put the nodes in right order
                break

            # Calculate the sum of current nodes and add carry
            node_sum = head_1.data + head_2.data + carry
            carry = 0

            # Check for carry condition and update the sum if necessary
            if node_sum >= 10:
                node_sum -= 10
                carry = 1
            sum_list.push(node_sum)  # Insert sum to output list

            # Move to next nodes for next iteration
            head_1 = head_1.next
            head_2 = head_2.next

    sum_list.show()  # Show the sum list
    return sum_list.head  # Return head to sum list


def add_linked_lists(ll1, ll2):
    """
    Method to add two integer numbers represented as linked lists
    :param ll1: linked list representation of first number
    :param ll2: linked list representation of second number
    :return: linked list representation of sum of the two numbers
    """
    # Reverse linked lists to order the nodes as ones, tens, thousands..
    ll1.reverse()
    ll2.reverse()

    # Get head reference of both LL
    head_1 = ll1.head
    head_2 = ll2.head

    # Add the two LLs
    output_ll_head = add_as_linked_lists(head_1, head_2)


# Test
ll1 = LinkedList([4, 5])
ll2 = LinkedList([3, 4, 5])
add_linked_lists(ll1, ll2)  # Output: 3->9->0

ll1 = LinkedList([6, 3])
ll2 = LinkedList([7])
add_linked_lists(ll1, ll2)  # Output: 7->0

ll1 = LinkedList([9, 9, 9])
ll2 = LinkedList([1])
add_linked_lists(ll1, ll2)  # Output: 1->0->0->0
