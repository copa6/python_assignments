"""
5. Given a string S, remove the vowels 'a', 'e', 'i', 'o', and 'u' from it, and return the new string.



Example 1:

Input: "leetcodeisacommunityforcoders"
Output: "ltcdscmmntyfrcdrs"
Example 2:

Input: "aeiou"
Output: ""
"""
######################################################################################################
"""
The following function removes vowels from an input string. 
There are two implementations. The first uses regular expression to substitute all vowels with an empty character in the
input string.
The second implementation replaces one vowel at a time from the input string.
"""
######################################################################################################

import re


def remove_vowels(s):
    """
    Function to remove all vowels from the input string using regular expressions
    :param s: input string
    :return: string with vowels removed
    """
    # Use regular expression to substitute aeiou with "" in the string
    # [] : indicates to match any of the entries inside
    # aeiou : Matches any of the vowels
    vowels_replaced_string = re.sub(r'[aeiou]', "", s)
    return vowels_replaced_string


def remove_vowels_v2(s):
    """
    Function to remove all vowels from the input string using inbuilt replace()
    :param s: input string
    :return: string with vowels removed
    """
    # Iterate over vowels replacing one vowel at a time from the input string
    for v in "aeiou":
        s = s.replace(v, "")
    return s


# Test
print(remove_vowels("leetcodeisacommunityforcoders"))  # Output: ltcdscmmntyfrcdrs
print(remove_vowels("aeiou"))  # Output:

print(remove_vowels_v2("leetcodeisacommunityforcoders"))  # Output: ltcdscmmntyfrcdrs
print(remove_vowels_v2("aeiou"))  # Output:
