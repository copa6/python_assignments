"""
1. K largest

Given an array Arr of N positive integers, find K largest elements from the array.  The output elements should be printed in decreasing order.

Example 1:

Input:
N = 5, K = 2
Arr[] = {12, 5, 787, 1, 23}
Output: 787 23
Explanation: 1st largest element in the
array is 787 and second largest is 23.
Example 2:

Input:
N = 7, K = 3
Arr[] = {1, 23, 12, 9, 30, 2, 50}
Output: 50 30 23
Explanation: 3 Largest element in the
array are 50, 30 and 23.
"""
######################################################################################################
"""
The following functions get the top k elements from an input array. 
There are two approaches - 
I. The function iterates for k iteration and extracts the element with maximum value on each iteration and places in an
output array. 
II. Sort the array in decending array, and return the first k elements 
"""
######################################################################################################


def get_top_k(arr, k):
    """
    Function to get top k elements from the input array
    :param arr: input array
    :param k: integer indicating top k elements to be returned
    :return: Array containing top k elements from the input
    """
    top_k = []  # Initialize empty list to store the output

    # Iterate for k iterations
    while k > 0:
        max_elem = max(arr)  # Extract maximum value from array
        top_k.append(max_elem)  # Append maximum value to the output array
        arr.remove(max_elem)  # Remove the maximum element from input array
        k -= 1  # decrement counter for next iteration

    return top_k


def get_top_k_v2(arr, k):
    """
    Function to get top k elements from the input array
    :param arr: input array
    :param k: integer indicating top k elements to be returned
    :return: Array containing top k elements from the input
    """
    sorted_arr = sorted(arr, reverse=True)  # Sort the input in descending order
    # Return the top k elements from the list
    return sorted_arr[:k]


# Test
print(get_top_k([12, 5, 787, 1, 23], 2))  # Output: [787 23]
print(get_top_k([1, 23, 12, 9, 30, 2, 50], 3))  # Output [50 30 23]

print(get_top_k_v2([12, 5, 787, 1, 23], 2))  # Output: [787 23]
print(get_top_k_v2([1, 23, 12, 9, 30, 2, 50], 3))  # Output [50 30 23]
