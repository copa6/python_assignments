"""
4.  Special Stack

Design a data-structure SpecialStack that supports all the stack operations like push(), pop(), isEmpty(), isFull() and
an additional operation getMin() which should return minimum element from the SpecialStack.
Your task is to complete all the functions, using stack data-Structure.


Example 1:

Input:
Stack: 18 19 29 15 16
Output: 15
"""


class SpecialStack:
    """
    Special Stack implementation in python
    """
    def __init__(self, size=100):
        """
        Initialize the special stack
        :param size: number of elements the stack can store
        """
        self.count = 0  # Initialize count to 0. This keeps count of number of elements in the stack
        self.minimums = []  # Initialize list to store the minimum value at every insert
        self.size = size  # Initialize size to class variable
        self.stack_array = []  # Initialize an empty array to store the stack

    def push(self, data):
        """
        Method to push an element into the stack
        :param data: value to be pushed to stack
        :return: None
        """
        # If stack is full, raise error
        if self.isFull():
            raise Exception("Stack Overflow!")
        else:
            # If stack is empty, mark the input value as minimum
            if self.isEmpty():
                minimum = data
            else:
                # Else store minimum as the minima of current value and last minima. We need to do this to ensure we
                # return the right minimum if the previous minimum is popped from the stack
                minimum = min(data, self.minimums[-1])
            self.stack_array.append(data)  # Insert the value into stack
            self.minimums.append(minimum)  # Insert the minimum value into the minimums list
            self.count += 1  # Increment counter to keep count of elements in stack

    def pop(self):
        """
        Method to pop an element off the top of the stack
        :return: None
        """

        # If stack is empty, print
        if self.isEmpty():
            print("Stack is empty!")
        else:
            self.stack_array = self.stack_array[:-1]  # Remove last element from stack array
            self.minimums = self.minimums[:-1]  # Remove last element from minimums array
            self.count -= 1  # Decrement counter to keep count of elements in stack

    def isEmpty(self):
        """
        Method to check if the stack is empty
        :return: None
        """
        # If the array count is 0, return True, else False
        if self.count == 0:
            return True
        else:
            return False

    def isFull(self):
        """
        Method to check if the array is full
        :return: None
        """
        # If count of elements in the array is same as size, return True, else return False
        if self.count == self.size:
            return True
        else:
            return False

    def getMin(self):
        """
        Method to get minima from the stack
        :return:
        """
        # Return last element from minimums array
        return self.minimums[-1]


# Test
stack = SpecialStack()
stack.push(18)
stack.push(19)
stack.push(29)
stack.push(15)
stack.push(16)

print(stack.getMin())  # Output: 15

stack.pop()
stack.pop()
print(stack.getMin())  # Output: 18
