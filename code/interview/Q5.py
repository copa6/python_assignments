"""
The following function works to obtain a palindromic array by using either of the following conditions -
1. Make no change  - Condition 0
2. Move last character of element at index at i to the first position of element at index i+1 - Condition 1
3. Move first character of element at index at i+1 to the last position of element at index i - Condition 2

Using this, we create the funciton with the following flow -
1. Find the length of the array.
2. Iterate over all elements of the array till the mid-point - In the updated version of the function, we iterate over
all elements instead.
3. For each element make the following updates -
    a. Move last character of element at index at i to the first position of element at index i+1
    b. Move first character of element at index at i+1 to the last position of element at index i
    c. Move first character of element at index at n-1-i(mirror of element at index i)  to the last position
       of element at index n-2-i(mirror of element at index i+1)
    d. Move last character of element at index at n-2-i(mirror of element at index i+1)  to the first position
       of element at index n-1-i(mirror of element at index i)

    This gives us the conditions (0,1,2) for current and it's mirror element
4. Check if any of the combinations of the current element and it's mirror are equal - This will require evaluating 9
conditions - 0-0, 0-1, 0-2, 1-0, 1-1, 1-2, 2-0, 2-1, 2-2
5. If any of the above condition apart from 0-0 results in True, update the corresponding elements in the main array to
the elements that resulted in equality.
6. If at any index, all the conditions are false, that means that the elements are unequal even after the
transformations - Thus we return False, as it is not possible to create the palindromic array.
7. If the entire array is traversed without encountering any mismatch, return True, as the array is made palindromic.
"""


def obtainPalindromicArray(arr):
    """
    Function to obtain a palindromic array, if possible, by swapping first and last elements of neighbouring elements
    :param arr: input array
    :return: True if it is possible to construct a palindromic array, False otherwise
    """
    array_len = len(arr)  # Get the length of array for iterating over all elements

    # Iterate over the array till mid-point - Updated this in the optimized one because I felt that this may be causing
    # issues
    for i in range(array_len//2):
        # Condition 0 - No updates
        left_elem = arr[i]  # Get the current element
        second_left_elem = arr[i+1]  # Get current element's next element
        right_elem = arr[array_len-1-i]  # Current element's mirror element
        second_right_elem = arr[array_len-2-i]  # Get mirror element's previous element

        # Condition 1 - Move current elements last to first of it's neighbour, and revers on the mirror
        left_elem_c1 = left_elem + second_left_elem[0]
        second_left_elem_c1 = second_left_elem[1:]
        right_elem_c1 = second_right_elem[-1] + right_elem
        second_right_elem_c1 = second_right_elem[:-1]

        # Condition 2 - Move neighbour's first to last of current element, and reverse on the mirror
        left_elem_c2 = left_elem[:-1]
        second_left_elem_c2 = left_elem[-1] + second_left_elem
        right_elem_c2 = right_elem[1:]
        second_right_elem_c2 = second_right_elem + right_elem[0]

        # Evaluating the 9 conditions. If match is in a condition other than 0-0, update the non 0 condition elements

        if left_elem == right_elem:  # 0-0
            continue

        elif left_elem == right_elem_c1:  # 0-1
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem == right_elem_c2:  # 0-2
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        elif left_elem_c1 == right_elem:  # 1-0
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            continue

        elif left_elem_c1 == right_elem_c1:  # 1-1
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem_c1 == right_elem_c2:  # 1-2
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        elif left_elem_c2 == right_elem:  # 2-0
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            continue

        elif right_elem_c1 == left_elem_c2:  # 2-1
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem_c2 == right_elem_c2:  # 2-2
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        # If none of the conditions evaluate to True, then a palindromic array can not be created, so we return False
        else:
            return False

    # If palindromic array creation is possible, return True
    return True


def obtainPalindromicArray_V2(arr):
    """
    Function to obtain a palindromic array, if possible, by swapping first and last elements of neighbouring elements
    :param arr: input array
    :return: True if it is possible to construct a palindromic array, False otherwise
    """
    array_len = len(arr)  # Get the length of array for iterating over all elements


    ############################################# This is the only change in optimized version #############################################
    # This will help evaluate the conditions and moves in the reverse direction as well. Previous code only worked in
    # the forward direction
    # Iterate over the all elements of the array
    for i in range(array_len-1):
    #######################################################################################################################################
        # Condition 0 - No updates
        left_elem = arr[i]  # Get the current element
        second_left_elem = arr[i + 1]  # Get current element's next element
        right_elem = arr[array_len - 1 - i]  # Current element's mirror element
        second_right_elem = arr[array_len - 2 - i]  # Get mirror element's previous element

        # Condition 1 - Move current elements last to first of it's neighbour, and revers on the mirror
        left_elem_c1 = left_elem + second_left_elem[0]
        second_left_elem_c1 = second_left_elem[1:]
        right_elem_c1 = second_right_elem[-1] + right_elem
        second_right_elem_c1 = second_right_elem[:-1]

        # Condition 2 - Move neighbour's first to last of current element, and reverse on the mirror
        left_elem_c2 = left_elem[:-1]
        second_left_elem_c2 = left_elem[-1] + second_left_elem
        right_elem_c2 = right_elem[1:]
        second_right_elem_c2 = second_right_elem + right_elem[0]

        # Evaluating the 9 conditions. If match is in a condition other than 0-0, update the non 0 condition elements

        if left_elem == right_elem:  # 0-0
            continue

        elif left_elem == right_elem_c1:  # 0-1
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem == right_elem_c2:  # 0-2
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        elif left_elem_c1 == right_elem:  # 1-0
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            continue

        elif left_elem_c1 == right_elem_c1:  # 1-1
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem_c1 == right_elem_c2:  # 1-2
            arr[i] = left_elem_c1
            arr[i + 1] = second_left_elem_c1
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        elif left_elem_c2 == right_elem:  # 2-0
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            continue

        elif right_elem_c1 == left_elem_c2:  # 2-1
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            arr[array_len - 1 - i] = right_elem_c1
            arr[array_len - 2 - i] = second_right_elem_c1
            continue

        elif left_elem_c2 == right_elem_c2:  # 2-2
            arr[i] = left_elem_c2
            arr[i + 1] = second_left_elem_c2
            arr[array_len - 1 - i] = right_elem_c2
            arr[array_len - 2 - i] = second_right_elem_c2
            continue

        # If none of the conditions evaluate to True, then a palindromic array can not be created, so we return False
        else:
            return False

    # If palindromic array creation is possible, return True
    return True


print(obtainPalindromicArray(["aa", "bab", "cde", "aba", "ab"]))  # True
print(obtainPalindromicArray(["palindrome"]))  # True
print(obtainPalindromicArray(["aaaaa", "aa"]))  # False


