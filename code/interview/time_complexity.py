
N = 5
# This loop would run for N iterations, but since we are doing i*2 at the end of every loop,
# it exponentially decay, giving a time complexity if logN
for i in range(1, N):
    counter = 0  # O(1)
    # In every iteration, this loop will run N times, as the counter is initialized to 0. Time complexity N
    while counter < N:
        counter += 1  # O(1)

    i *= 2


# Total time complexity = O(log N) * O(1) * O(N) * O(1) = O(N logN)
# Best and worst case complexity remain the same as the loop will always run for logN, and the inner while loop will
# run for N iterations every time.
