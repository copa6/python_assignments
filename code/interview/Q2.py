"""
Function to identify whether a string is beautiful or not.
A beautiful string is one where the number of characters occur in descending order, i.e., count of a >= count of b >=
count of c >= count of d...
The function follows the following steps -
1. Create a dictionary mapping all english alphabets to a count of zero
2. Sort the input string (This step would be redundant as we already have a sorted counts dicts create in step 1)
3. Get a count of each alphabet in the string using Counter
4. Update the counts in dictionary created in step 1
5. Initialize an empty variable to store the value of count of previous variable - Initialize it ot the max string
length constraint (51 here)
5. Iterate over all elements of the counts dict created in step, checking that the count of the previous alphabet is
less than or equal to that of the current alphabet. When the condition fails, return False, else return True.
"""


from collections import Counter  # Counter used for counting the characters in string


def isBeautifulString(s):
    """
    Funciton to identify is a string is beautiful
    :param s: input string
    :return: boolean indicating if a string is beautiful
    """
    ordered_string = "".join(sorted(s))  # Sort the input string alphabetically - Redundant
    alphabet_count = Counter(ordered_string)  # Count the occurrence of each alphabet in the string
    counts_dict = {k:0 for k in list("abcdefghijklmnopqrstuvwxyz")}  # Create an empty dictionary to store each alphabet and it's count. Initialize all counts to 0

    # Update count of alphabets present in the input string
    for k, v in alphabet_count.items():
        counts_dict[k] = v

    previous_val = 51  # As input constraint max input length is 50

    # Iteratively check if count of current alphabet is less than or equal to that of the previous alphabet
    for k, v in counts_dict.items():
        if v <= previous_val:
            previous_val = v  # If current alphabet count is less than previous, update previous and continue
        else:
            return False
    # If the entire sequence of characters is in decreasing order, return True
    return True


def isBeautifulString_pptimized(s):
    """
    Funciton to identify is a string is beautiful. Optimized to remove redundant steps
    :param s: input string
    :return: boolean indicating if a string is beautiful
    """
    # Create an empty dictionary to store each alphabet and it's count. Initialize all counts to 0
    counts_dict = {k: 0 for k in list("abcdefghijklmnopqrstuvwxyz")}

    # For each alphabet in the string, increment it's count by 1
    for k in list(s):
        counts_dict[k] += 1

    previous_val = 51  # As input constraint max input length is 50
    # Iteratively check if count of current alphabet is less than or equal to that of the previous alphabet
    for k, v in counts_dict.items():
        if v <= previous_val:
            previous_val = v  # If current alphabet count is less than previous, update previous and continue
        else:
            return False
    # If the entire sequence of characters is in decreasing order, return True
    return True


print(isBeautifulString("bbbaacdafe"))
print(isBeautifulString("aabbb"))
print(isBeautifulString("bbc"))
