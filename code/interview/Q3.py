"""
Function to count errors per file in logs.
The function works in the following steps -
1. Create an empty dictionary to store file names and their error counts.
2. Iterate over all .log files in the logs directory - Pathlib helps check all the subdirectories and find .log files
3. Read the contents of the log file into a string.
4. Use regular expressions to match all error lines in the file and extract their associated file name.
5. For each file, make an entry of the error count in the dictionary initialized in step 1.
6. Sort the dictionary by the count of errors - This gives the files from most errors to least. # This is redundant, as we do another sort later
7. Create a reverse mapping dictionary from error counts, to a list of file names - This will be used to sort the files
in lexicographical order.
8. For each count, sort the list of files and print the file name and associated error count.
"""

from pathlib import Path
import re
from collections import defaultdict


def count_error_in_logs(logdir='/root/devops/logs'):
    """
    Function to count errors per file in logs.
    :param logdir: Directory containing the log files. Default: /root/devops/logs
    :return: print files and theirs associated number of errors
    """
    error_count_dict = dict()  # Initialize a dictionary to store filename and associated error count

    # Regular expression to match error line -
    # ERROR : matches the term ERROR
    # \| : Followed by a pipe
    # ([a-zA-Z._0-9]*) : Matches filename which can contain alphabets(lower and upper case), numbers, dot and underscore
    error_logs_matcher = r'ERROR\|([a-zA-Z._0-9]*)'

    # Iterate over each .log file in the log directory
    for log_filepath in Path(logdir).rglob('*.log'):
        with open(log_filepath, 'r') as f:
            logs_data = f.read()  # Read the contents of the log file
            error_lines = re.findall(error_logs_matcher, logs_data)  # Find all error line matches in the file

            # For each error line match, make an entry of the error count in the error counts dictionary
            for error_file in error_lines:
                if error_file in error_count_dict:
                    error_count_dict[error_file] += 1
                else:
                    error_count_dict[error_file] = 1

    sorted_error_counts = {k: v for k, v in sorted(error_count_dict.items(), key=lambda item: item[1])}  # Redundant as we sort again later on line 38

    # Create a reverse mapping dictionary from error count, to list of files with that error
    count_error_dict = defaultdict(list)
    for k,v in sorted_error_counts.items():
        count_error_dict[v].append(k)

    # Sort the dictionary by keys (thus making line 47 redundant) to get the files from most errors to least
    for k, v in sorted(count_error_dict.items(), reverse=True):
        v.sort()  # Sort the file names lexicographically
        for filename in v:
            print(filename + " " + str(k))  # Pring file name and its associated errors count


# Test function call
count_error_in_logs()
