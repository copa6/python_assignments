"""
Function to add two digits of a number in the range 10-99.
The function works in the following steps -
1. Extract the last digit of the number by doing a modulo division by 10.
2. Extract the first digit by first doing a integer division(//) by 10, and then doing a modulo division of the
remaining value by 10 (now I realize we don't need to do this, as the integer division will give us the first digit).
3. Sum the two digits and return
"""


def addTwoDigits(n):
    """
    Function to add two digits of a number ranging from 10-99
    :param n: two digit integer number
    :return: sum of the two digits
    """
    num1 = n % 10  # Extract the last digit by doing a modulo division with 10
    num2 = (n//10) % 10  # Extract the first digit by doing an integer division with 10 (The modulo division is redundant here)

    # Return the sum of the two digits
    return num1 + num2


# FunctionCall
print(addTwoDigits(12))
