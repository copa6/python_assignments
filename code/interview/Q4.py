"""
Function to identify all the ip addresses in text files inside a directory.
The function works in the following steps -
1. Create an empty list to store all valid ip addresses.
2. Iterate over all .txt files in the input directory directory - Pathlib helps check all the subdirectories and find .txt files
3. Read the contents of the txt file into a string.
4. Use regular expression to match all the ip address like chunks in the file data.
5. For each ip address chunk found in the file, validate if each group is between 0-255 by splitting at dot and checking
6. Append all valid ip addresses to the list initialized in step 1.
7. Sort the list and print the results
"""
from pathlib import Path
import re


def find_all_ip_addresses():
    """
    Function to identify all the ip addresses in text files inside a directory.
    :return: list of all valid ip addresses sorted lexicographically
    """
    all_ips = []  # Initialize an empty dictionary to store the valid ip address

    # Regular expression to match an ip address like string -
    # \d : Indicates a digit
    # {1, 3} : Indicates presence of 1-3 elements of the type before {
    # \d{1,3} : Repeated 4 times to indicate the 4 groups in an ipv4 address
    # \. : Forms the dot separator between each group
    valid_ip_address_regexp = r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'

    # List all .txt files in the directory
    for p in Path('/root/devops').rglob('*.txt'):
        with open(p, 'r') as f:
            file_data = f.read()  # Read the contents of the file
            all_matched_ip = re.findall(valid_ip_address_regexp, file_data)  # Match all ip addresses in the file

            # Iterate over all ip address that are matched and check for validity
            for ip in all_matched_ip:
                if all([0<=int(pkt)<=255 for pkt in ip.split(".")]):  # Split the ip address at dot, and check if all the groups lie in 0-255 range
                    all_ips.append(ip)

    all_ips.sort()  # lexicographically sort all the ip addresses

    return all_ips
