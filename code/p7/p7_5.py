# coding=utf-8
"""
5. Given a non-empty array of integers, return the k most frequent elements.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]
Note:

You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
It's guaranteed that the answer is unique, in other words the set of the top k frequent elements is unique.
You can return the answer in any order.
"""
######################################################################################################
"""
Function to get k most frequent elements from the input array. 
The function work in the following steps - 
1. Use counter to get a count of each element in the input array into a dictionary.
2. Sort the dictionary by count of elements in descending order.
3. Return the top k keys of the dictionary.
"""
######################################################################################################
from collections import Counter  # Library import


def get_k_most_frequent(arr, k):
    """
    Function to get k most frequent elements of input array
    :param arr: input array
    :param k: int indicating the k most frequent elements to return
    :return: list containing top k elements of the input array
    """
    elem_counts = Counter(arr)  # Use counter to get count of each element of the input array

    # Sort the keys of counts dictionary by the value of count in reverse order
    sorted_elems = [k for k, v in sorted(elem_counts.items(), key=lambda item: item[1], reverse=True)]

    # Return the top k keys
    return sorted_elems[:k]


# Test
print(get_k_most_frequent([1,1,1,2,2,3], 2))  # Output: [1, 2]
print(get_k_most_frequent([1], 1))  # Output: [1, 2]
