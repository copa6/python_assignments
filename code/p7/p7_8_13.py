# coding=utf-8
"""
8. Merge two sorted linked lists and return it as a new sorted list.
The new list should be made by splicing together the nodes of the first two lists.



Example 1:


Input: l1 = [1,2,4], l2 = [1,3,4]
Output: [1,1,2,3,4,4]
Example 2:

Input: l1 = [], l2 = []
Output: []
Example 3:

Input: l1 = [], l2 = [0]
Output: [0]



OR

13.Merge two sorted linked lists
Last Updated: 10-09-2020

Write a SortedMerge() function that takes two lists, each of which is sorted in increasing order, and merges the two
together into one list which is in increasing order. SortedMerge() should return the new list. The new list should be
made by splicing together the nodes of the first two lists.
For example if the first linked list a is 5->10->15 and the other linked list b is 2->3->20, then SortedMerge() should
return a pointer to the head node of the merged list 2->3->5->10->15->20.
There are many cases to deal with: either ‘a’ or ‘b’ may be empty, during processing either ‘a’ or ‘b’ may run out
first, and finally, there’s the problem of starting the result list empty, and building it up while going through ‘a’
and ‘b’.

"""

######################################################################################################
"""
The following function merges two sorted linked lists such that the output retains the sort order.
It works in the following steps - 
1. Initialize and empty linked list to store the merge output.
2. Check if either of the list is empty and just mark the other list as the output
3. Starting at the head of each linked list, check which node has data of lower quantity
    - Mark that node as the starting point of the new linked list and move pointer of that node to the next node.
4. Iterate over all nodes of the linked list in this order, comparing the node values at each insert and inserting the 
node that has lower value
5. Once either of the nodes are empty, mark the remaining data from the other node as remaining elements of linked list
and return.
"""
######################################################################################################
from copy import deepcopy

class Node:
    """
    Class to define node of a linked list
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    """
    Class to implement a linked list in python
    """
    def __init__(self, data=None):
        """
        Initialize the linked list with None at it's head
        """
        self.head = None
        if data:
            self.create(data)

    def create(self, list_data):
        """
        Creates a linked list from input list of elements
        :param list_data: data to be put into linked list
        :return: None
        """
        for data in list_data[::-1]:
            self.push(data)

    def push(self, data):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param data: single element to be added to the linked list
        :return: None
        """
        node = Node(data)  # Create a new node instance
        self.push_node(node)

    def push_node(self, node):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param node: Node to be added to linked list
        :return: None
        """
        node.next = self.head  # Mark the node to be current head's previous node
        self.head = node  # Update reference of head to current node


    def show(self):
        """
        Helper method to print the linked list instance
        :return:
        """
        node = self.head
        print_output = []
        while node:
            print_output.append(str(node.data))
            node = node.next
        print("->".join(print_output))


def sortedMerge(ll1, ll2):
    """
    Function to merge two sorted linked lists such that the order of sort is maintained
    :param ll1: First linked list to merge
    :param ll2: Second linked list
    :return: head to the merged linked list
    """
    merged_list = LinkedList()  # Initialize and empty linked list to store the output

    # Get head references of the two input linked lists
    head_1 = ll1.head
    head_2 = ll2.head

    # If either head is none, mark the other LL as the output
    if head_1 is None:
        merged_list.head = head_2
    elif head_2 is None:
        merged_list.head = head_1
    else:
        # Identify the node with lower value and copy it as the starting of output LL.
        if head_1.data <= head_2.data:
            tail = deepcopy(head_1)
            head_1 = head_1.next  # Move reference to next node if first LL has lower value
        else:
            tail = deepcopy(head_2)
            head_2 = head_2.next  # Move reference to next node if second LL has lower value

        merged_list.head = tail  # Mark as starting of output LL
        while True:
            # If there is no data in either of the nodes, reference the remaining nodes of the other LL as the nodes
            # of output LL and break
            if head_1 is None:
                tail.next = head_2
                break
            if head_2 is None:
                tail.next = head_1
                break

            # Compare the data of the nodes and whichever is lower append that to the output LL. Move it's reference to
            # next node
            if head_1.data <= head_2.data:
                tail.next = head_1
                head_1 = head_1.next
            else:
                tail.next = head_2
                head_2 = head_2.next

            # Advance the tail to it's next node ( None )
            tail = tail.next

    merged_list.show()  # Show the merged list
    return merged_list.head


# Test
ll1 = LinkedList([1, 2, 4])
ll2 = LinkedList([1, 3, 4])
sorted_list_head = sortedMerge(ll1, ll2)  # Output: 1->1->2->3->4->4

ll1 = LinkedList([])
ll2 = LinkedList([])
sorted_list_head2 = sortedMerge(ll1, ll2)  # Output:

ll1 = LinkedList([])
ll2 = LinkedList([0])
sorted_list_head3 = sortedMerge(ll1, ll2)  # Output: 0

ll1 = LinkedList([2, 3, 5])
ll2 = LinkedList([5, 10, 15])
sorted_list_head4 = sortedMerge(ll1, ll2)  # Output: 2->3->5->5->10->15
