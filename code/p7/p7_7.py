"""
7. Given an array of integers nums, sort the array in ascending order.



Example 1:

Input: nums = [5,2,3,1]
Output: [1,2,3,5]
Example 2:

Input: nums = [5,1,1,2,0,0]
Output: [0,0,1,1,2,5]
"""

######################################################################################################
"""
Function to sort an array in ascending order in python. 
One implementation uses the sorted() inbuilt function in python to sort the input array. 
Second implementation is Quicksort implemented from scratch in python. It has the following flow - 
1. Start with the entire array - Starting index 0 and ending index at length of array-1
2. Place all elements smaller than the last elements of array to the left of last element.
3. Partition the array at index of last swap, and run steps 1 and 2 iteratively till all elements are sorted.
4. Return sorted array.
"""
######################################################################################################


def sort_integer_array_v1(arr):
    """
    Function to sort an input array using inbuild python sort() function
    :param arr: input array
    :return: sorted input array
    """
    return sorted(arr)


def partition_and_sort(arr, start_idx, end_idx):
    """
    Helper function to in-place sort input array between index start_idx and end_idx, by swapping all elements less than
    end_idx elements to the left
    :param arr: input array to be sorted
    :param start_idx: start index of sorting
    :param end_idx: end index of sorting
    :return: index of array where last swap was made
    """
    i = start_idx  # store starting index in iterator variable
    local_max = arr[end_idx]  # Store value of end index in variable for comparison

    # Iterate over the array from start_idx to end_idx
    for j in range(start_idx, end_idx):
        # If current element is smaller than or equal to last_idx, swap it with the i-th element
        if arr[j] <= local_max:
            arr[i], arr[j] = arr[j], arr[i]
            i = i + 1  # Increment i

    # This will ensure all elements to left of i are smaller than i-th element
    arr[i], arr[end_idx] = arr[end_idx], arr[i]  # Swap the element at end index with i'th element

    return i


def quicksort(arr, start_idx, end_idx):
    """
    Helper function to quicksort input array from start to end index
    :param arr: input array
    :param start_idx: Starting index of sort
    :param end_idx: End index of sort
    :return: None
    """
    # If array has only one element, return as is
    if len(arr) == 1:
        return arr

    # If starting index is less than ending index, partition and sort the array, followed by a recursive quicksort call
    # to both partitions
    if start_idx < end_idx:
        partition_idx = partition_and_sort(arr, start_idx, end_idx)
        quicksort(arr, start_idx, partition_idx - 1)  # Quicksort left partition
        quicksort(arr, partition_idx + 1, end_idx)  # Quicksort right partition


def sort_integer_array_v2(arr):
    """
    Function to sort the input array using Quicksort algorithm
    :param arr: input array
    :return: sorted array
    """
    arr_len = len(arr)  # Length of input array
    quicksort(arr, 0, arr_len-1)  # Sort the array from 0 to last index

    # Return sorted array
    return arr


# Test
print(sort_integer_array_v1([5,2,3,1]))  # Output: [1, 2, 3, 5]
print(sort_integer_array_v1([5,1,1,2,0,0]))  # Output: [0, 0, 1, 1, 2, 5]

print(sort_integer_array_v2([5,2,3,1]))  # Output: [1, 2, 3, 5]
print(sort_integer_array_v2([5,1,1,2,0,0]))  # Output: [0, 0, 1, 1, 2, 5]
