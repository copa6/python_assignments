"""
10. Given a sorted array and a number x, find the pair in array whose sum is closest to x
Last Updated: 12-07-2020
Given a sorted array and a number x, find a pair in array whose sum is closest to x.
Examples:

Input: arr[] = {10, 22, 28, 29, 30, 40}, x = 54
Output: 22 and 30

Input: arr[] = {1, 3, 4, 7, 10}, x = 15
Output: 4 and 10
"""

######################################################################################################
"""
Function to find a pair in an array whose sum is closest to a constant value x.
The function works in the following steps - 
1. Use inbuilt itertools library get all pairwise combinations in the input list.
2. Initialize minimum difference to infinity (placeholder)
3. For each pair, calculate the sum of the pair and its distance from constant x. 
4. If the sum is closer than any previous sum, update the minimum difference variable and place the pair in output list.
5. As an early stopping criteria, if difference between the sum and x is 0, stop the loop and return current elements 
"""
######################################################################################################

import math
from itertools import combinations


def get_array_sum_closest_to_x(arr, x):
    """
    Functions to identify pair from input array, whose sum is closest to a constant input x
    :param arr: input array containing integer elements
    :param x: Constant value x
    :return: pair of integers from arr, whose sum is closest to x
    """
    min_diff = math.inf  # Initialize a placeholder variable to store minimum difference
    closest_elems = []  # Initialize array to hold elements whose sum was the closest to x

    # Iterate over all pairwise combinations of the input array
    for n1, n2 in combinations(arr, 2):
        elem_sum = n1 + n2  # Calculate sum
        dist_from_x = abs(elem_sum-x)  # Calculate absolute difference of the sum from x

        # If the sum is closer to x than any of the last sum, update min_diff and store current pair in return list
        if dist_from_x < min_diff:
            min_diff = dist_from_x
            closest_elems = [n1, n2]

            # Early stopping criteria where sum of elements is equal to x
            if min_diff == 0:
                break

    return closest_elems


# test
print(get_array_sum_closest_to_x({10, 22, 28, 29, 30, 40}, 54))  # Output: [22, 30]
print(get_array_sum_closest_to_x({1, 3, 4, 7, 10}, 15))  # Output: [4, 10]

