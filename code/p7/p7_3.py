"""
3. Given a collection of numbers that might contain duplicates, return all possible unique permutations.

Example:

Input: [1,1,2]
Output:
[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]

"""
######################################################################################################
"""
Function to get all unique permutations of array elements with repeating elements. 
The function uses itertools library to get all permutations of the array, and then creates a set() to preserve only
unique elements.
"""
######################################################################################################


def get_array_permutations(arr):
    """
    Function to get unique permutations of input array
    :param arr: input array
    :return: list of unique permutations of the input array
    """
    from itertools import permutations  # Library import

    # Get all permutations of the array and convert to a set to preserve only unique permutations
    unq_combinations = set([c for c in permutations(arr)])

    # Convert each permutation to a list and return
    arr_combinations = [list(c) for c in unq_combinations]
    return arr_combinations


# Test
print(get_array_permutations([1, 2, 1]))  # Output: [[1, 2, 1], [2, 1, 1], [1, 1, 2]]
