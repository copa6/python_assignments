"""
12. Rearrange array in alternating positive & negative items with O(1) extra space | Set 1
Last Updated: 13-08-2019
Given an array of positive and negative numbers, arrange them in an alternate fashion such that every positive
number is followed by negative and vice-versa maintaining the order of appearance.
Number of positive and negative numbers need not be equal. If there are more positive numbers they appear at the
end of the array. If there are more negative numbers, they too appear in the end of the array.

Examples :

Input:  arr[] = {1, 2, 3, -4, -1, 4}
Output: arr[] = {-4, 1, -1, 2, 3, 4}

Input:  arr[] = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8}
output: arr[] = {-5, 5, -2, 2, -8, 4, 7, 1, 8, 0}
"""

######################################################################################################
"""
The following function rearranges the elements of the input array, such that the output has alternating positive and 
negative elements.  
It works in the follwong steps - 
1. Extract all positive and negative elements from the input array into two separate arrays.
2. Identify whether positive or negative elements are fewer and initialize iterator to that count. Initialize that many 
iterations
3. On each iteration, insert negative element at that index from negative array, followed by positive element at that
index from the positive array into an output array.
4. Once the iterations are done, extend the output array and insert remaining elements from both the arrays into it - 
NOTE - Step 4 will only insert elements from either positive or negative element array as all elements from the other 
are exhausted during the loop.
"""
######################################################################################################


def rearrange_array(input_arr):
    """
    Function to rearrange the elements of the input array, such that the output has alternating positive and
    negative elements.
    :param input_arr: input array containing positive and negative elements
    :return: Rearranged array such that it contains alternating positive and negative elements
    """
    positives = []  # Initialize empty list to store positive integers from input
    negatives = []  # Initialize empty list to store positive integers from input
    output_arr = []  # Initialize output array

    # Iterate over the array and place into positives or negatives
    for elem in input_arr:
        if elem >= 0:
            positives.append(elem)
        else:
            negatives.append(elem)

    # Initialize iterator to iterate over array with fewer elements
    iter_len = min(len(positives), len(negatives))
    for i in range(iter_len):
        # On each iteration insert a negative and a positive element from the two lists into the output. This ensures
        # alternating between positive and negative element
        output_arr.extend([negatives[i], positives[i]])

    # Extend the output array to accomodate the remaining elements of whichever list had more elements.
    # The other list here adds an empty array to the output and makes no difference to the output
    output_arr.extend(negatives[i+1:])
    output_arr.extend(positives[i+1:])

    return output_arr


# Test
print(rearrange_array([1, 2, 3, -4, -1, 4]))  # Output: [-4, 1, -1, 2, 3, 4]
print(rearrange_array([-5, -2, 5, 2, 4, 7, 1, 8, 0, -8]))  # Output: [-5, 5, -2, 2, -8, 4, 7, 1, 8, 0]
