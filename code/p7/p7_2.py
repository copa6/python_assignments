"""
2. Given a collection of distinct integers, return all possible permutations.

Example:

Input: [1,2,3]
Output:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]

"""
######################################################################################################
"""
Function to get permutations of array elements. 
The function uses inbuilt python library itertools to get permutation of array elements.
"""
######################################################################################################


def get_array_permutations(arr):
    """
    Function to get permutation of array elements
    :param arr: inpiut array
    :return: permutations of the input array elements
    """
    from itertools import permutations  # Library import
    arr_combinations = [list(c) for c in permutations(arr)]  # Use list comprehension to get all permutations of the array

    # Return array combinations
    return arr_combinations


# Test
print(get_array_permutations([1, 2, 3]))  # Output: [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
