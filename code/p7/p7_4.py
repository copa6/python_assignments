"""
4. Given a string s, return all the palindromic permutations (without duplicates) of it. Return an empty list if no palindromic permutation could be form.

Example 1:

Input: "aabb"
Output: ["abba", "baab"]
Example 2:

Input: "abc"
Output: []

"""
######################################################################################################
"""
Function to get all palindromic combinations of input string.
The function works in the following steps - 
1. Check if the string can be converted to a plaindromic one, by counting the number of times each character appears in
the string. If there is only one character that is present odd number of times, the string  can be converted to a 
palindrome. 
2. If the string can be converted to a palindrome. Use inbuilt library itertools to get all permutations of the input 
string.
3. For each permutation, check if it is palindromic by iterating over the characters and checking if mirror elements are 
equal, and store in list.
"""
######################################################################################################


def can_string_be_made_palindrome(s):
    """
    Function to check if a string, aor any of it's anagrams is a palindrome
    :param s: string to check for palindrome
    :return: boolean indicating the palindrome status of s
    """
    from collections import Counter
    character_counts_in_string = Counter(s)  # Use counter to get character counts in string s. Extract just the count values
    odd_counts = 0

    # Check all counts and add to odd_counts variable. If more than one odd counts exist, return false.
    for count in character_counts_in_string.values():
        if count%2 == 1:
            odd_counts += 1

        if odd_counts > 1:
            return False

    # If there are fewer than 1 odd characters in string, return True
    return True


def check_if_string_palindrome(s, s_len):
    """
    Check if input string is palindrome by iterating over each character and checking if mirror elements are equal.
    :param s: input string to check for palindrome
    :param s_len: length of the input string
    :return: boolean indicating if the string is palindromic
    """

    # Iterate over the characters of the string till mid-point
    for i in range(s_len//2):
        if s[i] != s[s_len-1-i]:  # Check if current element is equal to it's mirror. If any unequality is found, return False
            return False
    return True


def get_string_palindromes(s):
    """
    Function to get palindrome permutations of the input string
    :param s: input string
    :return: list containing the palindrome permutations of input string
    """
    from itertools import permutations  # Library import
    palindrome_combinations = []  # initialize empty list to store palindromic combinations of the input string
    is_palindrome_possible = can_string_be_made_palindrome(s)  # Check if input string can be converted to a palindrome

    # If possible to convert current string to palindrome, get combination
    if is_palindrome_possible:
        string_length = len(s)  # Get length of the string. Extracted once, as it needs to be used for each permutation check.

        # Get all unique permutations of the input string. We use set() to get unique combinations
        all_combinations = list(set(["".join(c) for c in permutations(s)]))

        # for each permutation of the string, check if it is a palindrome and add to output list.
        for c in all_combinations:
            if check_if_string_palindrome(c, string_length):
                palindrome_combinations.append(c)

    return palindrome_combinations


# test
print(get_string_palindromes("aabb"))  # Output: ['baab', 'abba']
print(get_string_palindromes("abc"))  # Output: []
