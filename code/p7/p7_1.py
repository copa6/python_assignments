"""
1. Write a function that reverses a string. The input string is given as an array of characters char[].

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

You may assume all the characters consist of printable ascii characters.
"""
######################################################################################################
"""
Function to reverse string given as an array of characters without creating additional array.
The code works in the following steps - 
1. Iterate over the array till mid-point.
2. For each element, swap it with it's mirror element (n-1-i) using a temporary variable - This uses O(1) space.

"""
######################################################################################################


def reverse_string(char_arr):
    """
    Function to reverse a string passed as character array, without creating a new array
    :param char_arr: input string passed as a character array
    :return: reversed array
    """
    arr_len = len(char_arr)  # This takes up additional O(1) memory but has been used to make the code readable. We can replace arr_len with len(char_arr) wherever it is used
    # Iterate till the array's mid point
    for i in range(arr_len//2):
        temp = char_arr[i]  # Store current element in a temp variable - O(1) memory
        char_arr[i] = char_arr[arr_len-1-i]  # Move mirror element value to current element
        char_arr[arr_len - 1 - i] = temp  # Store current element value in mirror

    # Return reversed array
    return char_arr


# Test:
print(reverse_string(["a", "b", "c", "d"]))  # Output: ['d', 'c', 'b', 'a']
print(reverse_string(["n", "ab", "ba", "d"]))  # Output: ['d', 'ba', 'ab', 'n']
