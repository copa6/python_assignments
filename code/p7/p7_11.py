"""
1. Find minimum difference between any two elements
Last Updated: 20-06-2018
Given an unsorted array, find the minimum difference between any pair in given array.

Examples :

Input  : {1, 5, 3, 19, 18, 25};
Output : 1
Minimum difference is between 18 and 19

Input  : {30, 5, 20, 9};
Output : 4
Minimum difference is between 5 and 9

Input  : {1, 19, -4, 31, 38, 25, 100};
Output : 5
Minimum difference is between 1 and -4
"""

######################################################################################################
"""
Function to find pair of elements in an array whose difference is the least.
First implementation works in the following steps - 
1. Initialize placeholder minimum difference to difference between maximum and minimum value of the array. All 
differences will be lesser than this
2. For each pairwise combination of elements in the list, calculcate their absolute difference.
3. If the difference between current two elements is lesser than any previous difference, update minimum difference
4. Return the minimum difference

The second implementation is more optimized and works on the logic that in a sorted array, the minimum difference will
always be between two consecutive elements. 
It works in the following steps - 
1. Sort the input array in ascending order.
2. Initialize minimum difference to be the difference between first two elements.
3. Iterate over the array from third to last element.
4. Calculate the difference between every element and the element preceding it. 
5. If the difference at any point is lower than previous minimum difference, update the minimum difference.
6. Return minimum difference.
"""
######################################################################################################


def find_min_diff_elems(arr):
    """
    Function to find minimum difference between any two elements in input array
    :param arr: input array
    :return: minimum difference between any two elements in input array
    """
    from itertools import combinations  # Library import
    min_diff = max(arr) - min(arr)  # Initialise min_diff as the difference between maximum and minimum value in the list

    # Calculate the absolute difference between each pair wise combination in the list. If difference is less than any
    # previously encountered difference update minimum difference
    for n1, n2 in combinations(arr, 2):
        diff = abs(n1-n2)
        if diff < min_diff:
            min_diff = diff

    # Return minimum difference
    return min_diff


def find_min_diff_elems_v2(arr):
    """
    Function to find minimum difference between any two elements in input array.
    The function works on the logic that in a sorted array, the minimum difference will always be between two
    consecutive elements.
    :param arr: input array
    :return: minimum difference between any two elements in input array
    """
    sorted_arr = sorted(arr)  # Sort the array in ascending order
    min_diff = sorted_arr[1] - sorted_arr[0]  # Initialize minimum difference as difference between first and second
    # elements

    # Iterate from third to last element in the list
    for i in range(2, len(sorted_arr)):
        diff = sorted_arr[i] - sorted_arr[i-1]  # Calculate the difference between current and previous element

        # If current difference is lesser than any previous difference, update minimum difference
        if diff < min_diff:
            min_diff = diff

    return min_diff


# test
print(find_min_diff_elems({1, 5, 3, 19, 18, 25}))  # Output: 1
print(find_min_diff_elems({30, 5, 20, 9}))  # Output: 4
print(find_min_diff_elems({1, 19, -4, 31, 38, 25, 100}))  # Output: 5


print(find_min_diff_elems_v2({1, 5, 3, 19, 18, 25}))  # Output: 1
print(find_min_diff_elems_v2({30, 5, 20, 9}))  # Output: 4
print(find_min_diff_elems_v2({1, 19, -4, 31, 38, 25, 100}))  # Output: 5
