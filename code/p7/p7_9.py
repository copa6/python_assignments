"""
9. Given a list of 24-hour clock time points in "HH:MM" format, return the minimum minutes difference between any two time-points in the list.


Example 1:

Input: timePoints = ["23:59","00:00"]
Output: 1
Example 2:

Input: timePoints = ["00:00","23:59","00:00"]
Output: 0
"""

######################################################################################################
"""
Function to get minimum difference in minutes difference between any two time-points in a list contating time as "HH:MM" 
string.
The function works in following steps - 
1. Convert each entry in the list to minutes by multiplying hours by 60 and adding the minutes. 
    - One caveat here, is that all 00:MM time formats need to be convertes using hour as 00, as well as 24, to ensure we
    are able to capture difference on both sides.
2. Use inbuilt library to get all pair-wise combinations of the minutes list
3. Initialize minimum difference to 12*60, as that could be the minimum difference between two time units
4. For each pair calculate the difference between elements, and if is lesser than the minimum difference, we replace 
minimum difference with it.
5. Return minimum difference.
"""
######################################################################################################


def get_minimum_minute_difference(time_points):
    """
    Function to get minimum difference between two time points passed as "HH:MM" in input list
    :param time_points: list containing time points as string in "HH:MM" format
    :return: minimum difference between a pair of time-points in the input list
    """
    from itertools import combinations  # Library import

    time_in_minutes = []  # Initialize list to store time in minutes as integer

    # Primary check to validate if there are any two times that are same in the list. If true, minimum difference will
    # be 0, and we return it.
    if len(time_points) != len(set(time_points)):
        return 0

    # For each time point, calculate the minutes as an integer by multiplying hours with 60 and adding the minutes
    for t in time_points:
        time_elems = t.split(":")  # Split the time string at : to extract hours and minutes

        # If hour element is 00, we need to calculate the minutes in both 24*60 and 00*60 format to be able to calculate
        # difference on both sides of the day
        if time_elems[0] == "00":
            mins = (24 * 60) + int(time_elems[1])
            time_in_minutes.append(mins)

        # Calculate time in minutes and append to list
        mins = (int(time_elems[0]) * 60) + int(time_elems[1])
        time_in_minutes.append(mins)

    min_diff = 60*12  # Initialize minimum time difference as 12*60, as that is the maximum possible time difference in a day

    # For each pair of minutes in the minutes list, calculate the absolute difference and update min_diff if found lesser
    for t1, t2 in combinations(time_in_minutes, 2):
        diff = abs(t1-t2)
        if diff < min_diff:
            min_diff = diff

    # Return minimum difference
    return min_diff


# Test
print(get_minimum_minute_difference(["23:59", "00:00"]))  # Output: 1
print(get_minimum_minute_difference(["23:59", "00:00", "23:59"]))  # Output: 0


