"""
6. Given two numbers represented by two linked lists of size N and M. The task is to return a sum list. The sum list is a linked list representation of the addition of two input numbers.

Example 1:

Input:
N = 2
valueN[] = {4,5}
M = 3
valueM[] = {3,4,5}
Output: 3 9 0
Explanation: For the given two linked
list (4 5) and (3 4 5), after adding
the two linked list resultant linked
list will be (3 9 0).
Example 2:

Input:
N = 2
valueN[] = {6,3}
M = 1
valueM[] = {7}
Output: 7 0
Explanation: For the given two linked
list (6 3) and (7), after adding the
two linked list resultant linked list
will be (7 0).

"""
######################################################################################################
"""
Function to add two integer numbers represented by linked lists. 
The function works in the following steps - 
1. Convert each linked list to an integer number.
2. Add the two numbers.
3. Convert the output sum number to a linked list and return.
"""
######################################################################################################


class Node:
    """
    Class to define node of a linked list
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    """
    Class to implement a linked list in python
    """
    def __init__(self, data=None):
        """
        Initialize the linked list with None at it's head
        """
        self.head = None
        if data:
            self.create(data)

    def create(self, list_data):
        """
        Creates a linked list from input list of elements
        :param list_data: data to be put into linked list
        :return: None
        """
        for data in list_data[::-1]:
            self.push(data)

    def push(self, data):
        """
        Adds one element to the linked list, updating the it as the next element of the previous head
        :param data: single element to be added to the linked list
        :return: None
        """
        node = Node(data)  # Create a new node instance
        node.next = self.head  # Mark the node to be current head's previous node
        self.head = node  # Update reference of head to current node

    def reverse(self):
        previous_node = None  # Mark previous instance as None. This will be used as the tail of reversed LL
        current_node = self.head  # Start with the head of the LL, and reverse .next mapping one node at a time

        # Iterate till the tail (None) of the LL
        while current_node:
            next_node = current_node.next  # Get a reference to the next element in the LL
            current_node.next = previous_node  # Reverse current node's next attribute to point to the previous node

            # Move one step to the right for next iteration
            previous_node = current_node
            current_node = next_node

        self.head = previous_node  # Mark the last node as the head of the reversed linked list

    def show(self):
        """
        Helper method to print the linked list instance
        :return:
        """
        node = self.head
        print_output = []
        while node:
            print_output.append(str(node.data))
            node = node.next
        print("->".join(print_output))


def convert_ll_to_int(ll):
    """
    Helper function to convert a linked list to integer
    :param ll: input linked list
    :return: integer number rep
    """
    node = ll.head  # get reference to linked list head
    digits = [node.data]  # Add head data to list for storing all digits of the number

    # Extract data from all nodes and insert into digits array
    while node.next:
        node = node.next
        digits.append(node.data)

    # convert digits list to integer by reversing and multiplying each index with a power of 10
    num = 0
    digits = digits[::-1]
    for i in range(len(digits)):
        num += digits[i] * 10**i
    return num


def convert_int_to_ll(int_num):
    """
    Helper method to convert and integer to a linked list
    :param int_num: integer number to be converted to a linked list
    :return: Linked list representation of the integer number
    """
    digits = []  # Initialize empty list to store digits of input number

    # ITeratively extract digits from the number by using modulo and floored division with 10
    while int_num > 0:
        digits.append(int_num%10)
        int_num //= 10

    digits = digits[::-1]  # Reverse the digits to insert into linked list
    ll = LinkedList(digits)  # create a linked lit with the reversed digits list
    return ll


def add_linked_lists(l1, l2):
    """
    Method to add two integer numbers represented as linked lists
    :param l1: linked list representation of first number
    :param l2: linked list representation of second number
    :return: linked list representation of sum of the two numbers
    """

    # convert the number to integer
    num1 = convert_ll_to_int(l1)
    num2 = convert_ll_to_int(l2)

    output_num = num1+num2  # Add
    output_ll = convert_int_to_ll(output_num)  # convert back to linked list
    output_ll.show()


# Test
ll1 = LinkedList([4, 5])
ll2 = LinkedList([3, 4, 5])
add_linked_lists(ll1, ll2)  # Output: 3->9->0

ll1 = LinkedList([6, 3])
ll2 = LinkedList([7])
add_linked_lists(ll1, ll2)  # Output: 7->0




######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
# WIP: Optimized method
######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
#
# def update_nodes(head, carry):
#     if head is not None:
#         data = head.data
#         data += carry
#         carry = 0
#         while data >= 10:
#             data -= 10
#             head.data = data
#             carry = 1
#             if head.next:
#                 head = head.next
#                 data = head.data + 1
#                 carry = 0
#         head.data = data
#     if carry:
#         new_node = Node(1)
#         head.next = new_node
#
#
# def add_as_linked_lists(head_1, head_2):
#     sum_list = LinkedList()
#     if head_1 is None:
#         sum_list.head = head_2
#     elif head_2 is None:
#         sum_list.head = head_1
#     else:
#         node_sum = head_1.data + head_2.data
#         carry = 0
#         if node_sum >= 10:
#             node_sum -= 10
#             carry = 1
#
#         sum_list.push(node_sum)
#         tail = sum_list.head
#
#         head_1 = head_1.next
#         head_2 = head_2.next
#         while True:
#             if head_1 is None and head_2 is None:
#                 break
#             if head_1 is None:
#                 update_nodes(head_2, carry)
#                 tail.next = head_2
#                 break
#             if head_2 is None:
#                 update_nodes(head_1, carry)
#                 tail.next = head_1
#                 break
#
#             node_sum = head_1.data + head_2.data + carry
#             carry = 0
#             if node_sum >= 10:
#                 node_sum -= 10
#                 carry = 1
#             sum_list.push(node_sum)
#             sum_list.show()
#             head_1 = head_1.next
#             head_2 = head_2.next
#             tail = sum_list.head
#
#     sum_list.reverse()
#     sum_list.show()
#     return sum_list.head
#
#
# def add_linked_lists_V2(ll1, ll2):
#     ll1.reverse()
#     ll2.reverse()
#     head_1 = ll1.head
#     head_2 = ll2.head
#
#     add_as_linked_lists(head_1, head_2)
#
#
#
#
# # Test
# ll1 = LinkedList([4, 5])
# ll2 = LinkedList([3, 4, 5])
# add_linked_lists_V2(ll1, ll2)  # Output: 3->9->0
#
# # ll1 = LinkedList([6, 3])
# # ll2 = LinkedList([7])
# # add_linked_lists_V2(ll1, ll2)  # Output: 7->0
# #
# #
# # ll1 = LinkedList([9, 9, 9])
# # ll2 = LinkedList([1])
# # add_linked_lists_V2(ll1, ll2)  # Output: 7->0