"""
14. Sort an array of 0s, 1s and 2s
Last Updated: 28-05-2020
Given an array A[] consisting 0s, 1s and 2s. The task is to write a function that sorts the given array.
The functions should put all 0s first, then all 1s and all 2s in last.

Examples:

Input: {0, 1, 2, 0, 1, 2}
Output: {0, 0, 1, 1, 2, 2}

Input: {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1}
Output: {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2}
"""
######################################################################################################
"""
Function to store an input array containing 0's, 1's and 2's. 
The function has the following steps - 
1. Count the number of 0,1 and 2 in the input array.
2. Create output array, by adding as many 0s, 1s and 2s in the list as their count, in that order.

NOTE - WE COULD ALSO USE THE INBUILT sorted() FUNCTION TO SORT THE LIST 
"""
######################################################################################################

from collections import Counter


def sort_arr(arr):
    """
    Function to sort a list that contains only 0s, 1s and 2,s
    :param arr: input array
    :return: sorted array with 0s coming first, followed by 1s and then 2s
    """
    elem_counts = Counter(arr)  # Count the number of times each element is present in the list

    # Create output list by multiplying and extending list of 0s, 1s and 2s
    sorted_arr = [0]*elem_counts[0] + [1]*elem_counts[1] + [2]*elem_counts[2]

    # Return sorted array
    return sorted_arr


# Test
print(sort_arr([0, 1, 2, 0, 1, 2]))  # Output: [0, 0, 1, 1, 2, 2]
print(sort_arr([0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1]))  # Output: [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2]
